#pragma once

#include "IWorld.h"
#include "../Scene/Base/SceneDataPtr.h"
#include "../Actor/Base/ActorManager.h"
#include <functional>

enum class EventMessage;

// ワールドクラス
class World : public IWorld {
public:
	// コンストラクタ
	explicit World(const SceneDataPtr& data);
	// 更新
	void update(float deltaTime);
	// 描画
	void draw() const;
	// メッセージ処理
	void handleMessage(EventMessage message, void* param);
	// フィールドの追加
	void addField(const FieldPtr& field);
	// カメラの追加
	void addCamera(const ActorPtr& camera);
	// ライトの追加
	void addLight(const ActorPtr& light);

	// アクターの追加
	virtual void addActor(ActorGroup group, const ActorPtr& actor) override;
	// アクターの検索
	virtual ActorPtr findActor(const std::string& name) override;
	// カメラの取得
	virtual ActorPtr getCamera() const override;
	// フィールドの取得
	virtual FieldPtr getField() const override;
	// フィールドの取得
	virtual SceneDataPtr getSceneData() const override;
	// メッセージの送信
	virtual void sendMessage(EventMessage message, void* param = nullptr)  override;
	// イベントリスナーの追加
	void addEventMessageListener(std::function<void(EventMessage, void*)> listener);
	// コピー禁止
	World(const World& other) = delete;
	World& operator = (const World& other) = delete;
private:
	// シーンデータ
	SceneDataPtr	mSceneData;
	// アクターマネージャー
	ActorManager	mActors;
	// ライト
	ActorPtr		mLight;
	// カメラ
	ActorPtr		mCamera;
	// フィールド
	FieldPtr		mField;
	// イベントリスナー
	std::function<void(EventMessage, void*)> mListener;
};

