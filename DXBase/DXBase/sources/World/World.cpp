#include "World.h"

#include "../Field/Field.h"

// コンストラクタ
World::World(const SceneDataPtr& data) :
	mSceneData(data),
	mListener([](EventMessage, void*) {}) {
}

// 更新
void World::update(float deltaTime) {
	// フィールドの更新処理
	mField->update(deltaTime);
	// カメラの更新処理
	mCamera->update(deltaTime);
	// ライトの更新処理
	mLight->update(deltaTime);
	// アクターの更新処理
	mActors.update(deltaTime);
}

// 描画
void World::draw() const {
	// フィールドの描画処理
	mField->draw();
	// カメラの描画処理
	mCamera->draw();
	// ライトの描画処理
	mLight->draw();
	// アクターの描画処理
	mActors.draw();
}

// メッセージ処理
void World::handleMessage(EventMessage message, void* param) {
	// ワールドのメッセージ処理
	mListener(message, param);
	// カメラのメッセージ処理
	mCamera->handleMessage(message, param);
	// ライトのメッセージ処理
	mLight->handleMessage(message, param);
	// アクターのメッセージ処理
	mActors.handleMessage(message, param);
}
 
// フィールドの追加
void World::addField(const FieldPtr& field) {
	mField = field;
}

// カメラの追加
void World::addCamera(const ActorPtr& camera) {
	mCamera = camera;
}

// ライトの追加
void World::addLight(const ActorPtr& light) {
	mLight = light;
}

// アクターの追加
void World::addActor(ActorGroup group, const ActorPtr& actor) {
	mActors.addActor(group, actor);
}

// アクターの検索
ActorPtr World::findActor(const std::string& name) {
	return mActors.findActor(name);
}

// カメラの取得
ActorPtr World::getCamera() const{
	return mCamera;
}

// フィールドの取得
FieldPtr World::getField() const {
	return mField;
}

SceneDataPtr World::getSceneData() const{
	return mSceneData;
}

// メッセージの送信
void World::sendMessage(EventMessage message, void* param) {
	handleMessage(message, param);
}

// イベントリスナーの追加
void World::addEventMessageListener(std::function<void(EventMessage, void*)> listener) {
	mListener = listener;
}


