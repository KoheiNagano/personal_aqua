#pragma once

#include "../Base/Scene.h"

// ゲームプレイシーン
class GamePlayScene : public Scene{
public:
	// コンストラクタ
	explicit GamePlayScene(const SceneDataPtr& data);
	// デストラクタ
	~GamePlayScene();
	// 初期化
	virtual void start() override;
	// 更新処理
	virtual void update(float deltaTime) override;
	// 描画処理
	virtual void draw() const override;
	// 終了処理
	virtual void end() override;
	// 次のシーンの取得
	virtual SceneID next() const override;
};
