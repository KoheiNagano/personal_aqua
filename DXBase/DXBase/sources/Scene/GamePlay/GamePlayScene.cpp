#include "GamePlayScene.h"

#include "../../Define.h"
#include "../../World/World.h"

#include "../../Field/Field.h"
#include "../../Actor/Base/ActorGroup.h"
#include "../../Actor/Camera/Camera.h"
#include "../../Actor/Light/Light.h"
#include "../../Actor/StageCreator/StageCreator.h"

#include "../../ResourceLoader/ResourceLoader.h"
#include "../../Scene/Base/SceneData.h"
#include "../../Input/InputMgr.h"

#include <memory>
#include <random>

// コンストラクタ
GamePlayScene::GamePlayScene(const SceneDataPtr& data) : Scene(data) {}

// デストラクタ
GamePlayScene::~GamePlayScene(){}

// 初期化
void GamePlayScene::start() {
	// ワールドの生成
	mWorld = std::make_shared<World>(mData);

	// ゲームプレイシーンに入った回数の更新
	mData->mSceneCount++;	
	// 現在のシーンのパラメータの初期化
	mData->getCurData().status.init();
		
	// ステージ要素を生成
	mWorld->addActor(ActorGroup::STAGE_CREATOR, std::make_shared<StageCreator>(mWorld.get(), mData));
	
	// フィールドの生成
	mWorld->addField(std::make_shared<Field>(ResourceLoader::GetInstance().getModelHandle(ModelID::STAGE), ResourceLoader::GetInstance().getModelHandle(ModelID::SKYDOME)));
	// ライトの生成
	mWorld->addLight(std::make_shared<Light>(mWorld.get(), Vector3(10.0f, 10.0f, 10.0f)));
	// カメラの生成
	mWorld->addCamera(std::make_shared<Camera>(mWorld.get(), mWorld->getSceneData()->getCurData().status.mParams.pos));

	// フォントサイズの設定
	SetFontSize(32);
}

// 更新処理
void GamePlayScene::update(float deltaTime) {
	//world更新
	mWorld->update(deltaTime);

	if (InputMgr::GetInstance().IsButtonUp(Buttons::BUTTON_START)) mIsEnd = true;
}

// 描画処理
void GamePlayScene::draw() const {
	//world描画
	mWorld->draw();

	// キー表示
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 25.0f, GetColor(255, 255, 255), "キャラ移動：方向キー");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 75.0f, GetColor(255, 255, 255), "カメラ回転：WASDキー");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 125.0f, GetColor(255, 255, 255), "ジャンプ：SPACEキー");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 175.0f, GetColor(255, 255, 255), "攻撃：Zキー");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 225.0f, GetColor(255, 255, 255), "連続攻撃：Zキー");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 275.0f, GetColor(255, 255, 255), "左シフト+右シフト：覚醒");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, 325.0f, GetColor(255, 255, 255), "覚醒時+左シフト：回避");

	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, SCREEN_SIZE.y - 100.0f, GetColor(255, 255, 255), "ENTERボタンで次のステージへ");
}

// 終了処理
void GamePlayScene::end() {}

// 次のシーンの取得
SceneID GamePlayScene::next() const {
	//３度プレイしたら終了
	if (mData->mSceneCount >= STAGE_NUM_MAX) {
		return SceneID::GameOver;
	}
	return SceneID::GamePlay;
}

