#pragma once

#include "../Base/Scene.h"

// ゲームオーバーシーン
class GameOverScene : public Scene{
public:
	// コンストラクタ
	explicit GameOverScene(const SceneDataPtr& data);
	// デストラクタ
	~GameOverScene();
	// 初期化
	virtual void start() override;
	// 更新処理
	virtual void update(float deltaTime) override;
	// 描画処理
	virtual void draw() const override;
	// 終了処理
	virtual void end() override;
	// 次のシーンの取得
	virtual SceneID next() const override;
};
