#include "GameOverScene.h"

#include "../../Define.h"
#include "../../Input/InputMgr.h"

// コンストラクタ
GameOverScene::GameOverScene(const SceneDataPtr& data) : Scene(data) {}

// デストラクタ
GameOverScene::~GameOverScene(){}

// 初期化
void GameOverScene::start() {
	SetFontSize(32);
}

// 更新処理
void GameOverScene::update(float deltaTime) {
	if (InputMgr::GetInstance().IsButtonUp(Buttons::BUTTON_START))mIsEnd = true;
}

// 描画処理
void GameOverScene::draw() const {
	// キー表示
	DrawFormatStringF(SCREEN_SIZE.x / 2, SCREEN_SIZE.y / 2, GetColor(255, 255, 255), "リザルト");
	DrawFormatStringF(SCREEN_SIZE.x - 500.0f, SCREEN_SIZE.y - 100.0f, GetColor(255, 255, 255), "Enterボタンでタイトルへ");
}

// 終了処理
void GameOverScene::end() {}

// 次のシーンの取得
SceneID GameOverScene::next() const {
	return SceneID::Title;
}
