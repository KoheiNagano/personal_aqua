#include "LoadingScene.h"

#include "../../ResourceLoader/ResourceLoader.h"

// コンストラクタ
LoadingScene::LoadingScene(const SceneDataPtr& data) : Scene(data) {
}

// デストラクタ
LoadingScene::~LoadingScene(){
	ResourceLoader::GetInstance().clearResources();
}

// 初期化
void LoadingScene::start(){
	// キャラクタモデルのロード
	ResourceLoader::GetInstance().loadModel(ModelID::PLAYER, "./resources/model/player/Vampire_A_Lusth.mv1");
	ResourceLoader::GetInstance().loadModel(ModelID::ENEMY, "./resources/model/enemy/Paladin.mv1");
	ResourceLoader::GetInstance().loadModel(ModelID::ENEMY_SWORD, "./resources/model/enemy/weapon/Sword_low.mv1");
	ResourceLoader::GetInstance().loadModel(ModelID::ENEMY_SHIELD, "./resources/model/enemy/weapon/Shield_low.mv1");
	ResourceLoader::GetInstance().loadModel(ModelID::BOSS, "./resources/model/enemy/boss/overload.mv1");
	ResourceLoader::GetInstance().loadModel(ModelID::BOSS_EQUIP, "./resources/model/enemy/boss/overload_equip.mv1");
	
	// 地形モデルのロード
	ResourceLoader::GetInstance().loadModel(ModelID::STAGE, "./resources/model/stage/village.mv1");
	ResourceLoader::GetInstance().loadModel(ModelID::STAGE_COLL, "./resources/model/stage/village.mv1");

	// スカイドームのロード
	ResourceLoader::GetInstance().loadModel(ModelID::SKYDOME, "./resources/model/skydome/skydome.mqo");
	
	// マスク用画像のロード
	ResourceLoader::GetInstance().loadMask(MaskID::METER, "./resources/textures/meter_mask.png");

	// メーター画像のロード
	ResourceLoader::GetInstance().loadTexture(TextureID::METER_FRAME, "./resources/textures/meter_frame.png");
	ResourceLoader::GetInstance().loadTexture(TextureID::METER_BLOOD, "./resources/textures/meter_blood.png");
	ResourceLoader::GetInstance().loadTexture(TextureID::METER_WINGS, "./resources/textures/meter_wings.png");

	// タイトル関連画像のロード
	ResourceLoader::GetInstance().loadTexture(TextureID::TITLE_BACK, "./resources/textures/title_back.jpg");
	ResourceLoader::GetInstance().loadTexture(TextureID::TITLE_CLOUD, "./resources/textures/title_cloud.png");
	ResourceLoader::GetInstance().loadTexture(TextureID::TITLE_LOGO, "./resources/textures/title_logo.png");
	ResourceLoader::GetInstance().loadTexture(TextureID::TITLE_BUTTON, "./resources/textures/title_button.png");

}

// 更新処理
void LoadingScene::update(float deltaTime){
	mIsEnd = true;
}

// 描画処理
void LoadingScene::draw() const{}

// 終了処理
void LoadingScene::end(){}

// 次のシーンの取得
SceneID LoadingScene::next() const{
	return SceneID::Title;
}
