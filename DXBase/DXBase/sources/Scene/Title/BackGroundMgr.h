#pragma once

#include "../Base/Scene.h"
#include "../../Math/Vector2.h"

// 背景マネージャー
class BackGroundMgr{
public:
	// コンストラクタ
	BackGroundMgr();
	// デストラクタ
	~BackGroundMgr();
	// 初期化
	void start();
	// 更新処理
	void update(float deltaTime);
	// 描画処理
	void draw() const;
	// 終了処理
	virtual void end();
private:
	// タイマー
	float mTimer;
	// スクロール背景の座標
	Vector2 mCloudPos;
};
