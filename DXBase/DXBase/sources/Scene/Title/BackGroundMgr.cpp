#include "BackGroundMgr.h"

#include "../../ResourceLoader/ResourceLoader.h"
#include "../../Define.h"
#include "../../Math/MathHelper.h"

// コンストラクタ
BackGroundMgr::BackGroundMgr() : mCloudPos(Vector2::Zero), mTimer(0.0f){}

// デストラクタ
BackGroundMgr::~BackGroundMgr(){}

// 初期化
void BackGroundMgr::start(){
	// 値の初期化
	mCloudPos = Vector2::Zero;
	mTimer = 0;
}

// 更新処理
void BackGroundMgr::update(float deltaTime){
	// タイマーの更新
	mTimer += deltaTime * 60;

	// スクロールの更新
	mCloudPos -= deltaTime * 60;
	// 画面外に出たら座標を戻す
	if (mCloudPos.x <= -ResourceLoader::GetInstance().getTexSize(TextureID::TITLE_CLOUD).x) {
		mCloudPos = 0;
	}
}

// 描画処理
void BackGroundMgr::draw() const{

	// 月を含む背景の描画
	DrawGraph(0, 0, ResourceLoader::GetInstance().getTextureHandle(TextureID::TITLE_BACK), TRUE);

	// スクロール背景の描画
	DrawGraphF(mCloudPos.x, 0, ResourceLoader::GetInstance().getTextureHandle(TextureID::TITLE_CLOUD), TRUE);
	DrawGraphF(mCloudPos.x + ResourceLoader::GetInstance().getTexSize(TextureID::TITLE_CLOUD).x, 0, ResourceLoader::GetInstance().getTextureHandle(TextureID::TITLE_CLOUD), TRUE);

	Vector2 drawPos;

	// タイトルロゴの描画
	drawPos = (SCREEN_SIZE - ResourceLoader::GetInstance().getTexSize(TextureID::TITLE_LOGO)) / 2;
	DrawGraphF(drawPos.x, drawPos.y - 200, ResourceLoader::GetInstance().getTextureHandle(TextureID::TITLE_LOGO), TRUE);

	// ボタンの描画
	drawPos = (SCREEN_SIZE - ResourceLoader::GetInstance().getTexSize(TextureID::TITLE_BUTTON)) / 2;
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, (MathHelper::Sin(mTimer) + 1) * 150);
	DrawGraphF(drawPos.x, drawPos.y + 200, ResourceLoader::GetInstance().getTextureHandle(TextureID::TITLE_BUTTON), TRUE);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
}

// 終了処理
void BackGroundMgr::end(){}
