#include "TitleScene.h"

#include "../../Input/InputMgr.h"
#include "../../Scene/Base/SceneData.h"

// コンストラクタ
TitleScene::TitleScene(const SceneDataPtr& data) : Scene(data) {}

// デストラクタ
TitleScene::~TitleScene(){}

// 初期化
void TitleScene::start() {
	mData->mSceneCount = 0;

	// 背景初期化
	mBackGround.start();
}

// 更新処理
void TitleScene::update(float deltaTime) {
	// 背景更新処理
	mBackGround.update(deltaTime);

	if (InputMgr::GetInstance().IsButtonUp(Buttons::BUTTON_START)) mIsEnd = true;
}

// 描画処理
void TitleScene::draw() const {
	// 背景描画
	mBackGround.draw();
}

// 終了処理
void TitleScene::end() {
	// 背景終了処理
	mBackGround.end();
}

// 次のシーンの取得
SceneID TitleScene::next() const {
	return SceneID::GamePlay;
}
