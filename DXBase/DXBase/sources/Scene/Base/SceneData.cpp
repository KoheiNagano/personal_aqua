#include "SceneData.h"

// コンストラクタ
SceneData::SceneData() : 
	mSceneCount(0),
	mData(std::unordered_map<SceneID, Data>()),
	mCurScene(SceneID::Null), 
	mPreScene(SceneID::Null){}

// デストラクタ
SceneData::~SceneData(){}

// データの追加
void SceneData::add(SceneID name, const Data & data){
	mData[name] = data;
}

// 現在のシーンの設定
void SceneData::setCurScene(SceneID name){
	mCurScene = name;
}

// 前回のシーンの設定
void SceneData::setPreScene(SceneID name){
	mPreScene = name;
}

// 現在のシーンの取得
SceneID SceneData::getCurScene(){
	return mCurScene;
}

// 前回のシーンの取得
SceneID SceneData::getPreScene(){
	return mPreScene;
}

// データの取得(シーン指定)
SceneData::Data & SceneData::getData(SceneID name) {
	return mData[name];
}

// 現在のシーンデータの取得
SceneData::Data & SceneData::getCurData(){
	return getData(mCurScene);
}
