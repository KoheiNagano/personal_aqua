#pragma once

#include "IScenePtr.h"
#include "SceneDataPtr.h"
#include "SceneID.h"

#include <string>
#include <unordered_map>

// シーンマネージャークラス
class SceneMgr {
public:
	// コンストラクタ
	SceneMgr();
	// 初期化
	void init();
	// 更新処理
	void update(float deltaTime);
	// 描画処理
	void draw() const;
	// 終了処理
	void end();
	// シーンの追加
	void add(SceneID name, const IScenePtr& scene, const std::string& file_path = "");
	// シーンの遷移
	void change(SceneID name);
	// シーンデータの取得
	SceneDataPtr& getData();
	// コピー禁止
	SceneMgr(const SceneMgr& other) = delete;
	SceneMgr operator = (const SceneMgr& other) = delete;
private:
	// シーンコンテナ
	std::unordered_map<SceneID, IScenePtr>mScenes;
	// 処理中のシーン
	IScenePtr mCurrentScene;
	// シーンデータ
	SceneDataPtr mData;
};
