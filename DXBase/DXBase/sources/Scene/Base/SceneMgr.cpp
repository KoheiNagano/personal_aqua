#include "SceneMgr.h"

#include "SceneData.h"
#include "SceneNull.h"

// コンストラクタ
SceneMgr::SceneMgr() :
	mCurrentScene(std::make_shared<SceneNull>()), mData(std::make_shared<SceneData>()){
}

// 初期化
void SceneMgr::init() {
	mScenes.clear();
}

// 更新処理
void SceneMgr::update(float deltaTime) {
	// 現在のシーンの更新
	mCurrentScene->update(deltaTime);
	// 現在のシーンの終了条件がtrueになった場合
	if (mCurrentScene->isEnd())	{
		// 次のシーンに遷移
		change(mCurrentScene->next());
	}
}

// 描画処理
void SceneMgr::draw() const {
	mCurrentScene->draw();
}

// 終了処理
void SceneMgr::end() {
	// 終了条件のリセット
	mCurrentScene->changed();
	// 現在のシーンの終了処理
	mCurrentScene->end();
	// NULLシーンを設定
	mCurrentScene = std::make_shared<SceneNull>();
}

// シーンの追加
void SceneMgr::add(SceneID name, const IScenePtr& scene, const std::string& file_path) {
	// シーンの追加
	mScenes[name] = scene;
	// シーンデータの追加
	mData->add(name, {file_path});
}

// シーンの遷移
void SceneMgr::change(SceneID name) {
	// 終了処理
	end();
	// 前回のシーンの設定
	mData->setPreScene(mData->getCurScene());
	// 現在のシーンの設定
	mData->setCurScene(name);
	// シーンの遷移
	mCurrentScene = mScenes[name];
	// シーンの初期化
	mCurrentScene->start();
}

// シーンデータの取得
SceneDataPtr & SceneMgr::getData() {
	return mData;
}
