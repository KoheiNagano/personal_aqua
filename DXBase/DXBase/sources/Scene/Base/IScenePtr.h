#pragma once

#include <memory>

//シーンインターフェイスポインタ
class IScene;
using IScenePtr = std::shared_ptr<IScene>;
