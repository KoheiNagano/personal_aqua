#include "SceneNull.h"

#include "SceneData.h"

// コンストラクタ
SceneNull::SceneNull() : Scene(std::make_shared<SceneData>()){}

// 初期化
void SceneNull::start() {}

// 更新処理
void SceneNull::update(float deltaTime){}

// 描画処理
void SceneNull::draw() const{}

// 終了処理
void SceneNull::end() {}

// 次のシーン
SceneID SceneNull::next() const { return SceneID::Null; }

