#pragma once

#include "Scene.h"

// NULLシーンクラス
class SceneNull : public Scene{
public:
	// コンストラクタ
	SceneNull();
	// 初期化
	virtual void start() override;
	// 更新処理
	virtual void update(float deltaTime) override;
	// 描画処理
	virtual void draw() const override;
	// 終了処理
	virtual void end() override;
	// 次のシーン
	virtual SceneID next() const override;
};

