#pragma once

#include <memory>

// シーンデータポインタ
class SceneData;
using SceneDataPtr = std::shared_ptr<SceneData>;
