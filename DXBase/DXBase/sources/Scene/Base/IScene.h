#pragma once

#include "SceneID.h"

//シーン抽象インターフェース
class IScene{
public:
	// 仮想デストラクタ
	virtual ~IScene(){}
	// 初期化
	virtual void start() = 0;
	// 更新
	virtual void update(float deltaTime) = 0;
	// 描画
	virtual void draw() const = 0;
	// 終了
	virtual void end() = 0;
	// 終了フラグ
	virtual bool isEnd() const = 0;
	// 遷移後処理
	virtual void changed() = 0;
	// 次のシーン
	virtual SceneID next() const = 0;
};

