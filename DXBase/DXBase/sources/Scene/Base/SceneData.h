#pragma once

#include "SceneID.h"
#include "../../Actor/Person/Player/Status/Status.h"

#include <unordered_map>
#include <string>

// シーンデータクラス
class SceneData {
	// シーンデータ構造体
	struct Data {
		// 読み込むファイル名
		std::string path;
		Status status;
	};
public:
	// コンストラクタ
	SceneData();
	// デストラクタ
	~SceneData();
	// データの追加
	void add(SceneID name, const Data& data);
	// 現在のシーンの設定
	void setCurScene(SceneID name);
	// 前回のシーンの設定
	void setPreScene(SceneID name);
	// 現在のシーンの取得
	SceneID getCurScene();
	// 前回のシーンの取得
	SceneID getPreScene();
	// データの取得(シーン指定)
	Data& getData(SceneID name);
	// 現在のシーンデータの取得
	Data& getCurData();
public:
	int mSceneCount;
private:
	// データコンテナ
	std::unordered_map<SceneID, Data> mData;
	// 現在のシーン
	SceneID mCurScene;
	// 前回のシーン
	SceneID mPreScene;
};
