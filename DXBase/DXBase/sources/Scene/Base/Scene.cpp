#include "Scene.h"

// コンストラクタ
Scene::Scene(const SceneDataPtr & data) : mData(data), mIsEnd(false){}

// デストラクタ
Scene::~Scene(){}

// シーンが終了したかどうか
bool Scene::isEnd() const{
	return mIsEnd;
}

// 遷移後処理
void Scene::changed(){
	mIsEnd = false;
}
