#pragma once

#include "IScene.h"
#include "SceneDataPtr.h"
#include "../../World/WorldPtr.h"

// シーンクラス
class Scene : public IScene {
public:
	// コンストラクタ
	explicit Scene(const SceneDataPtr& data);
	// デストラクタ
	~Scene();
	// シーンが終了したかどうか
	virtual bool isEnd() const override;
	// 遷移後処理
	virtual void changed() override;
protected:
	// ワールドポインタ
	WorldPtr		mWorld;
	// シーンデータポインタ
	SceneDataPtr	mData;
	// 終了条件
	bool mIsEnd;
};
