#include "StageLoader.h"

#include "../FileReader/CsvReader.h"
#include "../Math/Math.h"
#include "StageComponent.h"

// コンストラクタ
StageLoader::StageLoader(){}

// コンストラクタ
StageLoader::StageLoader(const std::string& file_name){
	// ファイルのロード
	load(file_name);
}

// デストラクタ
StageLoader::~StageLoader(){
	// 配列の解放
	mData.clear();
}

// ロード
void StageLoader::load(const std::string& file_name){
	// csvを読み込む
	CsvReader csv(file_name);
	for (int r = 0; r < csv.rows(); ++r) {
		std::vector<int> params = std::vector<int>();
		for (int c = 1; c < csv.columns(r); ++c) {
			params.push_back(csv.getf(r, c));
		}
		// プレイヤーの場合
		if (csv.gets(r, 0) == "player") {
			mData[StageComponent::Player].push_back(params);
		}
		// 敵の場合
		else if (csv.gets(r, 0) == "enemy") {
			mData[StageComponent::Enemy].push_back(params);
		}
	}
}

// 指定したインデックスのデータの取得
std::vector<std::vector<int>> StageLoader::getData(const StageComponent comp){
	return mData[comp];
}

// プレイヤーのデータを取得
std::vector<int> StageLoader::playerData(int index) {
	return  mData[StageComponent::Player][index];
}