#pragma once

#include <unordered_map>

struct Vector3;

enum class StageComponent;

//  ステージロードクラス
class StageLoader {
public:
	// コンストラクタ
	StageLoader();
	// コンストラクタ(ファイル名を指定してロード)
	explicit StageLoader(const std::string& file_name);
	// デストラクタ
	~StageLoader();
	// ロード
	void load(const std::string& file_name);
	// 指定したインデックスのデータの取得
	std::vector<std::vector<int>> getData(const StageComponent comp);
	// プレイヤーのデータを取得
	std::vector<int> playerData(int index = 0);
private:
	// csvデータ格納用の配列
	std::unordered_map<StageComponent, std::vector<std::vector<int>>> mData;
};