#include "Actor.h"
#include <algorithm>

#include "../Body/Base/DammyBody.h"
#include "../Body/Base/HitInfo.h"
#include "../../World/IWorld.h"
#include "../../Define.h"
#include "../../Field/Field.h"

// コンストラクタ
Actor::Actor(IWorld* world, const std::string& name, const Vector3& position, const IBodyPtr& body) :
	mWorld(world),
	mName(name),
	mPosition(position),
	mRotation(Matrix::Identity),
	mBody(body),
	mDead(false),
	mModelHandle(-1),
	mAnimation(-1),
	mMotionID(-1),
	mMotionSpeed(1.0f),
	mAlpha(0.0f),
	mHitNum(0),
	mPrePosition(Vector3::Zero),
	mCurPosition(Vector3::Zero){
}

// コンストラクタ
Actor::Actor(const std::string& name) :
	mWorld(nullptr),
	mName(name),
	mPosition(Vector3::Zero),
	mRotation(Matrix::Identity),
	mBody(std::make_shared<DammyBody>()),
	mDead(false),
	mModelHandle(-1),
	mAnimation(-1),
	mMotionID(-1),
	mMotionSpeed(1.0f),
	mAlpha(0.0f),
	mHitNum(0),
	mPrePosition(Vector3::Zero),
	mCurPosition(Vector3::Zero) {
}


// 更新
void Actor::update(float deltaTime) {
	onUpdate(deltaTime);
	eachChildren([&](Actor& child) { child.update(deltaTime); });

	mPrePosition = mPosition;
}

// 描画
void Actor::draw() const {
	onDraw();
	eachChildren([&](const Actor& child) { child.draw(); });
}


// 衝突判定
void Actor::collide(Actor& other) {
	if (isCollide(other)) {
		onCollide(other);
		other.onCollide(*this);
	}
	eachChildren([&](Actor& child) { child.collide(other); });
}


// 死亡しているか？
bool Actor::isDead() const {
	return mDead;
}

// 死亡する
void Actor::dead() {
	mDead = true;
}

// 名前を返す
const std::string& Actor::getName() const {
	return mName;
}

// 座標を返す
Vector3& Actor::position() {
	return mPosition;
}

// 回転行列を返す
Matrix& Actor::rotation() {
	return mRotation;
}

// 変換行列を返す
Matrix Actor::getPose() const {
	return Matrix(mRotation).Translation(mPosition);
}

// 子の検索
ActorPtr Actor::findCildren(const std::string& name) {
	return findCildren(
		[&](const Actor& actor) { return actor.getName() == name; });
}

// 子の検索
ActorPtr Actor::findCildren(std::function<bool (const Actor&)> fn) {
	const auto i = std::find_if(mChildren.begin(), mChildren.end(),
		[&](const ActorPtr& child) { return fn(*child); });
	if (i != mChildren.end()) {
		return *i;
	}
	for (const auto& child : mChildren) {
		const auto actor = child->findCildren(fn);
		if (actor != nullptr) {
			return actor;
		}
	}
	return nullptr;
}

// 子の衝突判定
void Actor::collideChildren(Actor& other) {
	eachChildren(
		[&](Actor& my) {
			other.eachChildren([&](Actor& target) { my.collide(target); });
		});
}

// 子の衝突判定
void Actor::collideSibling() {
	for (auto i = mChildren.begin(); i != mChildren.end(); ++i) {
		std::for_each(std::next(i), mChildren.end(),
			[&](const ActorPtr& actor) { (*i)->collide(*actor); });
	}
}

// 子の追加
void Actor::addChild(const ActorPtr& child) {
	mChildren.push_front(child);
}

// 子を巡回
void Actor::eachChildren(std::function<void (Actor&)>  fn) {
	std::for_each(
		mChildren.begin(), mChildren.end(),
		[&](const ActorPtr& child) { fn(*child); });
}

// 子を巡回 (const版）
void Actor::eachChildren(std::function<void (const Actor&)> fn) const {
	std::for_each(
		mChildren.begin(), mChildren.end(),
		[&](const ActorPtr& child) { fn(*child); });
}

// 子を削除
void Actor::removeChildren() {
	removeChildren([](Actor& child) { return child.isDead(); });
	eachChildren([](Actor& child) { child.removeChildren(); });
}

// 子を削除
void Actor::removeChildren(std::function<bool(Actor&)> fn) {
	mChildren.remove_if(
		[&](const ActorPtr& child) { return fn(*child); });
}

// 子を消去
void Actor::clearChildren() {
	mChildren.clear();
}

// モーションの設定
void Actor::setMotion(const int motion, const float speed) {
	mMotionID = motion;
	mMotionSpeed = speed;
}

// Worldの取得
IWorld* Actor::getWorld(){
	return mWorld;
}

// アニメーションの取得
Animation Actor::getAnim() {
	return mAnimation;
}

// 判定の形の取得
IBodyPtr Actor::getBody(){
	return mBody;
}

// 速度を返す
Vector3 Actor::velocity(){
	return mPosition - mPrePosition;
}

// 床に衝突したか
bool Actor::isOnFloor() const{
	return mHitNum > 0;
}

// メッセージ処理
void Actor::handleMessage(EventMessage message, void* param) {
	onMessage(message, param);
	eachChildren([&](Actor& child) { child.handleMessage(message, param); });
}

// フィールドとの衝突判定
void Actor::field() {
	// 移動前の座標を保存
	mPrePosition = mCurPosition;
	// 移動後の座標を算出
	mCurPosition = mPosition;

	// 衝突したポリゴンを格納（壁）
	std::vector<MV1_COLL_RESULT_POLY*> walls = std::vector<MV1_COLL_RESULT_POLY*>();
	// 衝突したポリゴンを格納（床）
	std::vector<MV1_COLL_RESULT_POLY*> floors = std::vector<MV1_COLL_RESULT_POLY*>();

	// プレイヤーの周囲にあるポリゴンを検出した結果が代入される当たり判定結果構造体
	MV1_COLL_RESULT_POLY_DIM HitDim = MV1CollCheck_Capsule(mWorld->getField()->modelHandle(), -1, Vector3::Vector3ToVECTOR(mCurPosition + mBody->points(0)), Vector3::Vector3ToVECTOR(mCurPosition + mBody->points(1)), mBody->radius());

	mHitNum = HitDim.HitNum;

	// 検出されたポリゴンが壁ポリゴンか床ポリゴン判断し、それぞれ配列に格納
	for (int i = 0; i < mHitNum; i++) {
		// 内積で傾き具合を計算
		float slope = Vector3::Dot(Vector3::VECTORToVector3(HitDim.Dim[i].Normal), Vector3::Up);
		
		//壁の場合
		if (slope < HIT_SLOPE_LIMIT) walls.push_back(&HitDim.Dim[i]);
		//床の場合
		else floors.push_back(&HitDim.Dim[i]);
	}	

	// 衝突した壁ポリゴンがない場合return
	if (!walls.empty()) wall_hit_check(mCurPosition, walls);

	// 床ポリゴンとの当たり判定
	if (!floors.empty()) floor_hit_check(mCurPosition, floors);

	// 検出したプレイヤーの周囲のポリゴン情報を開放する
	MV1CollResultPolyDimTerminate(HitDim);

	// 新しい座標を保存する
	mPosition = mCurPosition;
}

// 壁との衝突判定
void Actor::wall_hit_check(Vector3 & pos, std::vector<MV1_COLL_RESULT_POLY*> walls) {
	// 衝突フラグ
	bool hitFlag = false;

	// 移動ベクトル
	Vector3 dir = mCurPosition - mPrePosition;

	// 移動したかどうかで処理を分岐
	if (dir.Horizontal().Length() <= 0) return;

		// 壁ポリゴンの数だけ繰り返し
	for (int i = 0; i < walls.size(); i++) {
		// ポリゴンとプレイヤーが当たっていなかったら次のカウントへ
		if (!(hitFlag = hit_check_cap_tri(mCurPosition, walls[i]->Position))) continue;

		// 壁に当たったら壁に遮られない移動成分分だけ移動する
		// プレイヤーをスライドさせるベクトル
		Vector3 polyNormal = Vector3::VECTORToVector3(walls[i]->Normal);

		// 進行方向ベクトルと壁ポリゴンの法線ベクトルに垂直なベクトルを算出
		Vector3 slideVec = Vector3::Cross(dir, polyNormal);

		// 算出したベクトルと壁ポリゴンの法線ベクトルに垂直なベクトルを算出、これが
		// 元の移動成分から壁方向の移動成分を抜いたベクトル
		slideVec = Vector3::Cross(polyNormal, slideVec);

		// それを移動前の座標に足したものを新たな座標とする
		mCurPosition = mPrePosition + slideVec;

		// 新たな移動座標で壁ポリゴンと当たっていないかどうかを判定する
		for (int j = 0; j < walls.size(); j++) {
			// 当たっていたらループから抜ける
			if (hit_check_cap_tri(mCurPosition, walls[j]->Position)) break;
		}
	}
	
	// 壁に当たっていたら壁から押し出す処理を行う
	if (!hitFlag) return;

	// 壁ポリゴンの数だけ繰り返し
	for (int i = 0; i < walls.size(); i++) {
		// プレイヤーと当たっているかを判定
		if (!hit_check_cap_tri(mCurPosition, walls[i]->Position)) continue;

		// 当たっていたら規定距離分プレイヤーを壁の法線方向に移動させる
		mCurPosition = mCurPosition + Vector3::VECTORToVector3(walls[i]->Normal);

		// 移動した上で壁ポリゴンと接触しているかどうかを判定
		for (int j = 0; j < walls.size(); j++) {
			// 当たっていたらループを抜ける
			if (hit_check_cap_tri(mCurPosition, walls[j]->Position)) break;
		}
	}
}

// 床との衝突判定
void Actor::floor_hit_check(Vector3 & pos, std::vector<MV1_COLL_RESULT_POLY*> floors) {
	// 床ポリゴンに当たったかどうかのフラグを倒しておく
	bool hitFlag = false;

	float MaxY = 0.0f;

	// 床ポリゴンの数だけ繰り返し
	for (int i = 0; i < floors.size(); i++) {
		// 走っている場合は頭の先からそこそこ低い位置の間で当たっているかを判定( 傾斜で落下状態に移行してしまわない為 )
		HITRESULT_LINE LineRes = HitCheck_Line_Triangle(Vector3::Vector3ToVECTOR(mCurPosition), Vector3::Vector3ToVECTOR(mCurPosition + Vector3::Up * PERSON_HEIGHT), floors[i]->Position[0], floors[i]->Position[1], floors[i]->Position[2]);

		// 当たっていなかったら何もしない
		if (LineRes.HitFlag == FALSE) continue;

		// 既に当たったポリゴンがあり、且つ今まで検出した床ポリゴンより低い場合は何もしない
		if (hitFlag && MaxY > LineRes.Position.y) continue;

		// ポリゴンに当たったフラグを立てる
		hitFlag = true;

		// 接触したＹ座標を保存する
		MaxY = LineRes.Position.y;
	}

	// 床ポリゴンに当たったかどうかで処理を分岐
	if (!hitFlag) return;

	// 接触したポリゴンで一番高いＹ座標をプレイヤーのＹ座標にする
	mCurPosition.y = MaxY;
}

// カプセルとポリゴンとの衝突判定
bool Actor::hit_check_cap_tri(const Vector3& actor, const VECTOR poly[]){
	return HitCheck_Capsule_Triangle(Vector3::Vector3ToVECTOR(actor + mBody->points(0)), Vector3::Vector3ToVECTOR(actor + mBody->points(1)), mBody->radius(), poly[0], poly[1], poly[2]);
}

// メッセージ処理
void Actor::onMessage(EventMessage, void*) {}

// 更新
void Actor::onUpdate(float) {}

// 描画
void Actor::onDraw() const {
	mBody->transform(getPose())->draw();
}

// 衝突した
void Actor::onCollide(Actor&) {}

// 衝突判定
bool Actor::isCollide(Actor& other) const {
	return mBody->transform(getPose())->isCollide(*other.getBody()->transform(other.getPose()).get(), HitInfo());
}
