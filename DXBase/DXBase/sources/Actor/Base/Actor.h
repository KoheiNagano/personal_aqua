#pragma once

#include "DxLib.h"

#include "ActorPtr.h"
#include "../../Math/Math.h"

#include "../Body/Base/IBodyPtr.h"
#include "../Body/Base/DammyBody.h"

#include "../../Animation/Animation.h"
#include <string>
#include <memory>
#include <functional>
#include <forward_list>
#include <vector>

class IWorld;

enum class EventMessage;

class Actor {
public:
	// コンストラクタ
	Actor(IWorld* world, const std::string& name, const Vector3& position = Vector3::Zero, const IBodyPtr& body = std::make_shared<DammyBody>());
	// コンストラクタ
	explicit Actor(const std::string& name = "none");
	// 仮想デストラクタ
	virtual ~Actor() {}
	// 更新
	void update(float deltaTime);
	// 描画
	void draw() const;
	// 衝突処理
	void collide(Actor& other);
	// 死亡しているか？
	bool isDead() const;
	// 死亡する
	void dead();
	// 名前を返す
	const std::string& getName() const;
	// 座標を返す
	Vector3& position();
	// 回転行列を返す
	Matrix& rotation();
	// 変換行列を返す
	Matrix getPose() const;
	// 子の検索
	ActorPtr findCildren(const std::string& name);
	// 子の検索
	ActorPtr findCildren(std::function<bool (const Actor&)> fn);
	// 子同士の衝突
	void collideChildren(Actor& other);
	// 兄弟同士の衝突判定
	void collideSibling();
	// 子の追加
	void addChild(const ActorPtr& child);
	// 子を巡回
	void eachChildren(std::function<void(Actor&)> fn);
	// 子を巡回(const版)
	void eachChildren(std::function<void(const Actor&)> fn) const;
	// 子を削除する
	void removeChildren(std::function <bool(Actor&)> fn);
	// 子を削除する
	void removeChildren();
	// 子を消去
	void clearChildren();	
	// モーションの設定
	void setMotion(const int motion, const float speed);
	// Worldの取得
	IWorld* getWorld();
	// アニメーションの取得
	Animation getAnim() ;
	// 判定の形の取得
	IBodyPtr getBody();
	// 速度を返す
	Vector3 velocity();
	// 床に衝突したか
	bool isOnFloor() const;
	// メッセージ処理
	void handleMessage(EventMessage message, void* param);
	// コピー禁止
	Actor(const Actor& other) = delete;
	Actor& operator = (const Actor& other) = delete;
protected:
	// フィールドとの衝突判定
	void field();
private:
	// 壁との衝突判定
	void wall_hit_check(Vector3& pos, std::vector<MV1_COLL_RESULT_POLY*> walls);
	// 床との衝突判定
	void floor_hit_check(Vector3& pos, std::vector<MV1_COLL_RESULT_POLY*> floors);
	// カプセルとポリゴンとの衝突判定
	bool hit_check_cap_tri(const Vector3& actor, const VECTOR poly[]);
private:
	// メッセージ処理
	virtual void onMessage(EventMessage message, void* param);
	// 更新
	virtual void onUpdate(float deltaTime);
	// 描画
	virtual void onDraw() const;
	// 衝突した
	virtual void onCollide(Actor& other);
	// 衝突判定
	bool isCollide(Actor& other) const;

protected:
	// ワールド
	IWorld*	mWorld;
	// 名前
	std::string	mName;
	// 座標
	Vector3	mPosition;
	// 回転
	Matrix	mRotation;
	// 衝突判定
	IBodyPtr mBody;
	// 死亡フラグ
	bool mDead;
	// モデルハンドル
	unsigned int mModelHandle;
	// モーション制御クラス
	Animation mAnimation;
	// モーションID
	int	mMotionID;
	// モーション速度
	float mMotionSpeed;
	// 透明度
	float mAlpha;
	// 移動前の座標
	Vector3	mPrePosition;							
	// 現在の座標
	Vector3	mCurPosition;
	// 衝突したポリゴンの数
	int	mHitNum;
private:
	// 子アクター
	std::forward_list<ActorPtr> mChildren;
};
