#include "ActorManager.h"
#include "ActorGroup.h"

// コンストラクタ
ActorManager::ActorManager() {
	initialize();
}

// 初期化
void ActorManager::initialize() {
	// アクターグループの登録
	mActors[ActorGroup::STAGE_CREATOR] = std::make_shared<Actor>();
	mActors[ActorGroup::PLAYER] = std::make_shared<Actor>("PlayerManager");
	mActors[ActorGroup::PLAYER_ATTACK_RANGE] = std::make_shared<Actor>();
	mActors[ActorGroup::ENEMY] = std::make_shared<Actor>("EnemyManager");
	mActors[ActorGroup::ENEMY_ATTACK_RANGE] = std::make_shared<Actor>();
	mActors[ActorGroup::Enemy_Spawner] = std::make_shared<Actor>();
	mActors[ActorGroup::EFFECT] = std::make_shared<Actor>();
	root_.clearChildren();
	root_.addChild(mActors[ActorGroup::EFFECT]);
	root_.addChild(mActors[ActorGroup::PLAYER_ATTACK_RANGE]);
	root_.addChild(mActors[ActorGroup::ENEMY]);
	root_.addChild(mActors[ActorGroup::ENEMY_ATTACK_RANGE]);
	root_.addChild(mActors[ActorGroup::Enemy_Spawner]);
	root_.addChild(mActors[ActorGroup::PLAYER]);
	root_.addChild(mActors[ActorGroup::STAGE_CREATOR]);
}

// 更新
void ActorManager::update(float deltaTime) {
	root_.update(deltaTime);
	collide();
	root_.removeChildren();
}

// 描画
void ActorManager::draw() const {
	root_.draw();
}

// アクターの追加
void ActorManager::addActor(ActorGroup group, const ActorPtr& actor) {
	mActors[group]->addChild(actor);
}

// アクターの検索
ActorPtr ActorManager::findActor(const std::string& name) {
	return root_.findCildren(name);
}

// メッセージ処理
void ActorManager::handleMessage(EventMessage message, void* param) {
	root_.handleMessage(message, param);
}

// 衝突判定
void ActorManager::collide() {
	// 衝突するグループの指定
	//actors_[ActorGroup::Player]->collideChildren(*actors_[ActorGroup::Enemy]);
	mActors[ActorGroup::PLAYER]->collideChildren(*mActors[ActorGroup::ENEMY_ATTACK_RANGE]);
	mActors[ActorGroup::PLAYER_ATTACK_RANGE]->collideChildren(*mActors[ActorGroup::ENEMY]);
}
