#include "Player_AttackRange.h"
#include "../../Body/BoundingSphere.h"

// コンストラクタ
Player_AttackRange::Player_AttackRange(IWorld * world, const Vector3 & position) :
	Actor(world, "Player_AttackRange", position, std::make_shared<BoundingSphere>(Vector3::Zero, 10.0f)),
	mLifeTimer(0){}

// 更新処理
void Player_AttackRange::onUpdate(float deltaTime){
	// 一定時間経過で消滅
	mLifeTimer += deltaTime;
	if (mLifeTimer >= LIFE_TIME) dead();
}
