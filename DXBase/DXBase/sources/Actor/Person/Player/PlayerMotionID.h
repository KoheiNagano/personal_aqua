#pragma once

// プレイヤーモデルのモーションの列挙
enum class PlayerMotionID {
	NORMAL_IDLE = 0,
	NORMAL_WALK = 1,
	NORMAL_RUN = 2,
	NORMAL_JUMP = 3,
	POWER_UP = 4,
	AWAKE_IDLE = 5,
	AWAKE_WALK = 6,
	AWAKE_RUN = 7,
	AWAKE_JUMP = 8,
	PUNCH_1 = 9,
	PUNCH_2 = 10,
	PUNCH_3 = 11,
	KICK_1 = 12,
	KICK_2 = 13,
	KICK_3 = 14,
	KICK_4 = 15,
	ATTACK_SPECIAL = 16,
	ATTACK_LONG_RANGE = 17,
	ROLL = 18,
	BIT = 19,
	REACTION = 20,
	INJURED = 21,
	DYING = 22,
};