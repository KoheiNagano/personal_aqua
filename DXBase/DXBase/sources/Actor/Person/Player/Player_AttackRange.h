#pragma once

#include "../../Base/Actor.h"

// 攻撃判定
class Player_AttackRange : public Actor {
public:
	// コンストラクタ
	Player_AttackRange(IWorld* world, const Vector3& position);
private:
	// 更新処理
	virtual void onUpdate(float deltaTime) override;
private:
	const float LIFE_TIME = 0.5f;
	float mLifeTimer;
};