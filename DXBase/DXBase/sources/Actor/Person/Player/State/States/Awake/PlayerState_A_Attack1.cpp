#include "PlayerState_A_Attack1.h"

#include "../../../Player_AttackRange.h"
#include "../../../../../../Define.h"

PlayerState_A_Attack1::PlayerState_A_Attack1(Vector3& pos, Matrix& mat) : PlayerState(pos, mat), mStartPos(Vector3::Zero) {}

void PlayerState_A_Attack1::unique_init(Actor & actor){
	actor.getWorld()->addActor(ActorGroup::PlayerAttackRange, std::make_shared<Player_AttackRange>(actor.getWorld(), mPosition + Vector3::Up * 20 + actor.getPose().Backward() * 15 * 2));
	change_motion(actor, PlayerMotionID::KICK_1);
	mStartPos = mPosition;
}

void PlayerState_A_Attack1::update(Actor & actor, const float&  deltaTime){
	mPosition += Vector3::Down * GRAVITY;

	mPosition = Vector3::Lerp(mPosition, mStartPos + actor.getPose().Backward() * 15, 0.4f);
	
	if (actor.getAnim().isAnimEnd()) {
		change(PlayerState_Enum::A_IDLE);
	}
	else {
		if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CIRCLE)) change(PlayerState_Enum::A_ATTACK2);
	}
}

void PlayerState_A_Attack1::collide(const Actor & other)
{
}

void PlayerState_A_Attack1::end(){}

void PlayerState_A_Attack1::key_input(){}

void PlayerState_A_Attack1::pad_input(){}



