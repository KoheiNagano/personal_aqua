#pragma once

#include "../../PlayerState.h"

class PlayerState_A_Attack1 : public PlayerState {
public:
	PlayerState_A_Attack1(Vector3& pos, Matrix& mat);

	virtual void unique_init(Actor & actor) override;

	virtual void update(Actor & actor, const float& deltaTime) override;

	virtual void collide(const Actor & other) override;

	virtual void end() override;
	// キー入力処理
	virtual void key_input() override;
	// パッド入力処理
	virtual void pad_input() override;
private:
	Vector3 mStartPos;
};