#include "PlayerState_N_Attack3.h"

#include "../../../Player_AttackRange.h"
#include "../../../../../../Define.h"

PlayerState_N_Attack3::PlayerState_N_Attack3(Vector3& pos, Matrix& mat) : PlayerState(pos, mat), mStartPos(Vector3::Zero) {}

void PlayerState_N_Attack3::unique_init(Actor & actor) {
	actor.getWorld()->addActor(ActorGroup::PlayerAttackRange, std::make_shared<Player_AttackRange>(actor.getWorld(), mPosition + Vector3::Up * 20 + actor.getPose().Backward() * 15 * 2));
	change_motion(actor, PlayerMotionID::PUNCH_3);
	mStartPos = mPosition;
}

void PlayerState_N_Attack3::update(Actor & actor, const float& deltaTime) {
	mPosition += Vector3::Down * GRAVITY;

	mPosition = Vector3::Lerp(mPosition, mStartPos + actor.getPose().Backward() * 15, 0.4f);

	if (actor.getAnim().isAnimEnd()) {
		change(PlayerState_Enum::N_IDLE);
	}
}

void PlayerState_N_Attack3::collide(const Actor & other) {}

void PlayerState_N_Attack3::end() {}

void PlayerState_N_Attack3::key_input() {}

void PlayerState_N_Attack3::pad_input() {

}
