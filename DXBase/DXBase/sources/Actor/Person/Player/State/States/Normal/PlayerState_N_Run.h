#pragma once

#include "../../PlayerState.h"

class PlayerState_N_Run : public PlayerState {
public:
	PlayerState_N_Run(Vector3& pos, Matrix& mat);

	virtual void unique_init(Actor & actor) override;

	virtual void update(Actor & actor, const float& deltaTime) override;

	virtual void collide(const Actor & other) override;

	virtual void end() override;
	// キー入力処理
	virtual void key_input() override;
	// パッド入力処理
	virtual void pad_input() override;

//private:
//	bool move(Actor & actor, float deltaTime);

};