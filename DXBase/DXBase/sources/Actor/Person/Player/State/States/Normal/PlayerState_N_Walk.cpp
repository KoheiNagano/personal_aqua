#include "PlayerState_N_Walk.h"

#include "../../../../../../Define.h"

PlayerState_N_Walk::PlayerState_N_Walk(Vector3 & pos, Matrix & mat) : PlayerState(pos, mat) {}

void PlayerState_N_Walk::unique_init(Actor & actor){
	change_motion(actor, PlayerMotionID::NORMAL_WALK);
}

void PlayerState_N_Walk::update(Actor & actor, const float & deltaTime){
	mPosition += Vector3::Down * GRAVITY;

	//if (InputMgr::GetInstance().IsKeyDown(KeyCode::Z)) change(PlayerState_Enum::ATTACK);
	//if (InputMgr::GetInstance().IsKeyDown(KeyCode::X)) change(PlayerState_Enum::ATTACK);

	if (InputMgr::GetInstance().InputVectorL().Length() <= 0) {
		change(PlayerState_Enum::N_IDLE);
	}
	else if (InputMgr::GetInstance().InputVectorL().Length() > 0.5f){
		change(PlayerState_Enum::N_RUN);
	}
	else {
		move(actor, deltaTime, 1.0f);
	}
}

void PlayerState_N_Walk::collide(const Actor & other) {
	if (other.getName() == "Enemy_AttackRange") {
		change(PlayerState_Enum::DAMAGE);
	}
}

void PlayerState_N_Walk::end(){
}

void PlayerState_N_Walk::key_input(){
}

void PlayerState_N_Walk::pad_input(){
}

//bool PlayerState_N_Walk::move(Actor & actor, float deltaTime){
//	auto camera = actor.getWorld()->getCamera();
//	if (camera == nullptr) return false;
//
//	mVelocity = (camera->getPose().Forward() * InputMgr::GetInstance().InputVectorL().z + camera->getPose().Right() * InputMgr::GetInstance().InputVectorL().x) * Vector3(1, 0, 1);
//
//	Vector3 forward_cross_target = Vector3::Cross(mRotation.Forward(), -mVelocity.Normalize());
//	float up_dot_cross = Vector3::Dot(mRotation.Up(), forward_cross_target);
//	mAngle = Vector3::Angle(mRotation.Forward(), -mVelocity.Normalize()) * MathHelper::Sign(up_dot_cross);
//
//	mRotation *= Matrix::CreateFromAxisAngle(mRotation.Up(), mAngle);
//	mRotation.NormalizeRotationMatrix();
//
//	mPosition += mVelocity * 1.0f;
//}
