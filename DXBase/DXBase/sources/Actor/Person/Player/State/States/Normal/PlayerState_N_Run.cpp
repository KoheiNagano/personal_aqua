#include "PlayerState_N_Run.h"

#include "../../../../../../Define.h"

PlayerState_N_Run::PlayerState_N_Run(Vector3& pos, Matrix& mat) : PlayerState(pos, mat) {}

void PlayerState_N_Run::unique_init(Actor & actor){
	change_motion(actor, PlayerMotionID::NORMAL_RUN);	
}

void PlayerState_N_Run::update(Actor & actor, const float&  deltaTime){
	mPosition += Vector3::Down * GRAVITY;

	if (InputMgr::GetInstance().InputVectorL().Length() <= 0.5f) {
		change(PlayerState_Enum::N_WALK);
	}
	else {
		move(actor, deltaTime, 1.5f);
	}
}

void PlayerState_N_Run::collide(const Actor & other) {
	if (other.getName() == "Enemy_AttackRange") {
		change(PlayerState_Enum::DAMAGE);
	}
}

void PlayerState_N_Run::end() { }

void PlayerState_N_Run::key_input(){
	if (InputMgr::GetInstance().IsKeyDown(KeyCode::SPACE)) change(PlayerState_Enum::N_JUMP);
	if (InputMgr::GetInstance().IsKeyDown(KeyCode::Z)) change(PlayerState_Enum::N_ATTACK1);
	if (InputMgr::GetInstance().IsKeyDown(KeyCode::X)) change(PlayerState_Enum::N_ATTACK2);
}

void PlayerState_N_Run::pad_input(){
	if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CROSS))  change(PlayerState_Enum::N_JUMP);
	if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CIRCLE)) change(PlayerState_Enum::N_ATTACK1);
	//if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_LEFT)) change(PlayerState_Enum::ATTACK2);
}

//bool PlayerState_N_Run::move(Actor & actor, float deltaTime) {
//	auto camera = actor.getWorld()->getCamera();
//	if (camera == nullptr) return false;
//
//	mVelocity = (camera->getPose().Forward() * InputMgr::GetInstance().InputVectorL().z + camera->getPose().Right() * InputMgr::GetInstance().InputVectorL().x) * Vector3(1, 0, 1);
//
//	Vector3 forward_cross_target = Vector3::Cross(mRotation.Forward(), -mVelocity.Normalize());
//	float up_dot_cross = Vector3::Dot(mRotation.Up(), forward_cross_target);
//	mAngle = Vector3::Angle(mRotation.Forward(), -mVelocity.Normalize()) * MathHelper::Sign(up_dot_cross);
//
//	mRotation *= Matrix::CreateFromAxisAngle(mRotation.Up(), mAngle);
//	mRotation.NormalizeRotationMatrix();
//
//	mPosition += mVelocity * 1.5f;
//}
