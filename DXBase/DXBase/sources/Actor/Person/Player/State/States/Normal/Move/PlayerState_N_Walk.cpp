#include "PlayerState_N_Walk.h"

#include "../../../../PlayerMotionID.h"

// コンストラクタ
PlayerState_N_Walk::PlayerState_N_Walk(Vector3 & pos, Matrix & mat) : PlayerState_N_MoveBase(pos, mat) {}

// 各状態独自の初期化
void PlayerState_N_Walk::onUniqueInit(Actor & actor){
	// モーションの変更
	change_motion(actor, PlayerMotionID::NORMAL_WALK);
}

// 各状態独自の初期化
void PlayerState_N_Walk::onUpdate(Actor & actor, float deltaTime){
	// 移動処理
	move(actor, deltaTime, 1.0f);
}

