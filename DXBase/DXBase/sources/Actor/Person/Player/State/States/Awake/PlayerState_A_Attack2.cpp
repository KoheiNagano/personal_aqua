#include "PlayerState_A_Attack2.h"

#include "../../../Player_AttackRange.h"
#include "../../../../../../Define.h"

PlayerState_A_Attack2::PlayerState_A_Attack2(Vector3& pos, Matrix& mat) : PlayerState(pos, mat), mStartPos(Vector3::Zero){}

void PlayerState_A_Attack2::unique_init(Actor & actor){
	actor.getWorld()->addActor(ActorGroup::PlayerAttackRange, std::make_shared<Player_AttackRange>(actor.getWorld(), mPosition + Vector3::Up * 20 + actor.getPose().Backward() * 15 * 2));
	change_motion(actor, PlayerMotionID::KICK_2);
	mStartPos = mPosition;
}

void PlayerState_A_Attack2::update(Actor & actor, const float& deltaTime) {
	mPosition += Vector3::Down * GRAVITY;

	mPosition = Vector3::Lerp(mPosition, mStartPos + actor.getPose().Backward() * 15, 0.4f);

	if (actor.getAnim().isAnimEnd()) {
		change(PlayerState_Enum::A_IDLE);
	}
	if (!actor.getAnim().isAnimEnd()) {
		if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CIRCLE)) change(PlayerState_Enum::A_ATTACK3);
	}
}

void PlayerState_A_Attack2::collide(const Actor & other)
{
}

void PlayerState_A_Attack2::end() {}

void PlayerState_A_Attack2::key_input(){}

void PlayerState_A_Attack2::pad_input(){

}
