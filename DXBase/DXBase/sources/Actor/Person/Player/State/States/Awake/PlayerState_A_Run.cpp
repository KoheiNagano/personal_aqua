#include "PlayerState_A_Run.h"

#include "../../../../../../Define.h"

PlayerState_A_Run::PlayerState_A_Run(Vector3& pos, Matrix& mat) : PlayerState(pos, mat) {}

void PlayerState_A_Run::unique_init(Actor & actor) {
	change_motion(actor, PlayerMotionID::AWAKE_RUN);
}

void PlayerState_A_Run::update(Actor & actor, const float&  deltaTime) {
	mPosition += Vector3::Down * GRAVITY;

	if (InputMgr::GetInstance().InputVectorL().Length() <= 0.5f) {
		change(PlayerState_Enum::A_WALK);
	}
	else {
		move(actor, deltaTime, 2.0f);
	}
}

void PlayerState_A_Run::collide(const Actor & other){
	if (other.getName() == "Enemy_AttackRange") {
		change(PlayerState_Enum::DAMAGE);
	}
}

void PlayerState_A_Run::end() { }

void PlayerState_A_Run::key_input() {
	// �W�����v
	if (InputMgr::GetInstance().IsKeyDown(KeyCode::SPACE)) change(PlayerState_Enum::A_JUMP);
	// �K�[�h
	else if (InputMgr::GetInstance().IsKeyDown(KeyCode::R_SHIFT)) change(PlayerState_Enum::ROLL);
	// �U��1
	else if (InputMgr::GetInstance().IsKeyDown(KeyCode::Z)) change(PlayerState_Enum::A_ATTACK1);
	// �U��2
	else if (InputMgr::GetInstance().IsKeyDown(KeyCode::X)) change(PlayerState_Enum::A_ATTACK2);
	// �o����Ԃɐ؂�ւ�
	else if (awake_key()) change(PlayerState_Enum::NORMAL);
}

void PlayerState_A_Run::pad_input() {
	// �W�����v
	if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CROSS))  change(PlayerState_Enum::A_JUMP);
	// �K�[�h
	else if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_R1)) change(PlayerState_Enum::ROLL);
	// �U��1
	else if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CIRCLE)) change(PlayerState_Enum::A_ATTACK1);
	// �U��2
	else if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_SQUARE)) change(PlayerState_Enum::A_ATTACK2);
	// �o����Ԃɐ؂�ւ�
	else if (awake_pad()) change(PlayerState_Enum::NORMAL);
}
