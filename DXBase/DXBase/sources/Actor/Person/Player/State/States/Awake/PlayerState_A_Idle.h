#pragma once

#include "../../PlayerState.h"

// 待機状態
class PlayerState_A_Idle : public PlayerState {
public:
	// コンストラクタ
	PlayerState_A_Idle(Vector3& pos, Matrix& mat);
	// 各状態固有の初期化
	virtual void unique_init(Actor & actor) override;
	// 更新処理
	virtual void update(Actor & actor, const float& deltaTime) override;

	virtual void collide(const Actor & other) override;
	// 終了時の処理
	virtual void end() override;
	// キー入力処理
	virtual void key_input() override;
	// パッド入力処理
	virtual void pad_input() override;
};