#include "PlayerState_A_Idle.h"

#include "../../../../../../Define.h"

// コンストラクタ
PlayerState_A_Idle::PlayerState_A_Idle(Vector3& pos, Matrix& mat) : PlayerState(pos, mat){}

// 各状態固有の初期化
void PlayerState_A_Idle::unique_init(Actor & actor){
	change_motion(actor, PlayerMotionID::AWAKE_IDLE);
}

// 更新処理
void PlayerState_A_Idle::update(Actor & actor, const float& deltaTime) {
	if (InputMgr::GetInstance().InputVectorL().Length() > 0) change(PlayerState_Enum::A_WALK);

	mPosition += Vector3::Down * GRAVITY;
}

void PlayerState_A_Idle::collide(const Actor & other){
	if (other.getName() == "Enemy_AttackRange") {
		change(PlayerState_Enum::DAMAGE);
	}
}

// 終了時の処理
void PlayerState_A_Idle::end(){}

// キー入力処理
void PlayerState_A_Idle::key_input(){
	// ジャンプ
	if (InputMgr::GetInstance().IsKeyDown(KeyCode::SPACE)) change(PlayerState_Enum::A_JUMP);
	// ガード
	else if (InputMgr::GetInstance().IsKeyDown(KeyCode::R_SHIFT)) change(PlayerState_Enum::ROLL);
	// 攻撃1
	else if (InputMgr::GetInstance().IsKeyDown(KeyCode::Z)) change(PlayerState_Enum::A_ATTACK1);
	// 攻撃2
	else if (InputMgr::GetInstance().IsKeyDown(KeyCode::X)) change(PlayerState_Enum::A_ATTACK2);
	// 覚醒状態に切り替え
	else if (awake_key()) change(PlayerState_Enum::NORMAL);
}

// パッド入力処理
void PlayerState_A_Idle::pad_input() {
	// ジャンプ
	if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CROSS))  change(PlayerState_Enum::A_JUMP);
	// ガード
	else if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_R1)) change(PlayerState_Enum::ROLL);
	// 攻撃1
	else if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_CIRCLE)) change(PlayerState_Enum::A_ATTACK1);
	// 攻撃2
	else if (InputMgr::GetInstance().IsButtonDown(Buttons::BUTTON_SQUARE)) change(PlayerState_Enum::A_ATTACK2);
	// 覚醒状態に切り替え
	else if (awake_pad()) change(PlayerState_Enum::NORMAL);
}







