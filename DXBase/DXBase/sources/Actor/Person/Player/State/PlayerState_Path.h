#pragma once

// ステートのパス
#include "States/Normal/PlayerState_Normal.h"
#include "States/Normal/Move/PlayerState_N_Idle.h"
#include "States/Normal/Move/PlayerState_N_Walk.h"
#include "States/Normal/Move/PlayerState_N_Run.h"
#include "States/Normal/PlayerState_N_Roll.h"
#include "States/Normal/PlayerState_N_Jump.h"
#include "States/Normal/Attack/PlayerState_N_Attack.h"
#include "States/Normal/PlayerState_N_Damage.h"
#include "States/Normal/PlayerState_N_Guard.h"

#include "States/Awake/PlayerState_Awake.h"
#include "States/Awake/Move/PlayerState_A_Idle.h"
#include "States/Awake/Move/PlayerState_A_Walk.h"
#include "States/Awake/Move/PlayerState_A_Run.h"
#include "States/Awake/PlayerState_A_Jump.h"
#include "States/Awake/PlayerState_A_Quick.h"
#include "States/Awake/Attack/PlayerState_A_Attack.h"
