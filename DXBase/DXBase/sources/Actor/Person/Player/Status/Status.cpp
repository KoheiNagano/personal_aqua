#include "Status.h"

#include "../../../../ResourceLoader/ResourceLoader.h"
#include "../../../../Math/MathHelper.h"

#include <string>
#include <algorithm>

static const int METER_POS = 64;
 
// コンストラクタ
Status::Status() : mParams({ 0, 0 }) {}

// コンストラクタ
Status::Status(float maxHP) : mParams({ maxHP, 0 }) {}

// デストラクタ
Status::~Status(){}

// 初期化
void Status::init(){
	mParams.hp = 50;
}

// プレイヤーの座標データの登録
void Status::setPosData(Vector3 pos, Vector3 pos_min, Vector3 pos_max){
	// データを代入
	mParams.pos = pos;
	mParams.pos_min = pos_min;
	mParams.pos_max = pos_max;
}

// ダメージ計算
void Status::addDamage(float damage){
	mParams.hp = MathHelper::Clamp(mParams.hp - damage, 0, 100);
}

// コンボ計算
void Status::addCombo(){
	mParams.combo += 1;
}

// 描画処理
void Status::draw()const{
	// マスク関連の描画処理
	CreateMaskScreen();
	DrawMask(METER_POS, METER_POS, ResourceLoader::GetInstance().getMaskHandle(MaskID::METER), DX_MASKTRANS_BLACK);
	DrawGraph(METER_POS, METER_POS + 256.0f / 100.0f * (100 - mParams.hp), ResourceLoader::GetInstance().getTextureHandle(TextureID::METER_BLOOD), TRUE);
	DeleteMaskScreen();

	// フレームの描画
	DrawGraph(METER_POS, METER_POS, ResourceLoader::GetInstance().getTextureHandle(TextureID::METER_FRAME), TRUE);
	DrawGraph(-METER_POS, -METER_POS, ResourceLoader::GetInstance().getTextureHandle(TextureID::METER_WINGS), TRUE);
	//DrawFormatString(25, 75, GetColor(255, 255, 255), "HP : %d COMBO : %d", params_.hp, params_.combo);
}
