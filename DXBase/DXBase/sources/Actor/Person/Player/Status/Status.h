#pragma once

#include <DxLib.h>

#include "../../../../Math/Vector3.h"

// ステータスクラス
class Status {
public:
	// ステータスのパラメータ
	struct Params{
		float hp;
		int combo;
		// プレイヤー初期座標
		Vector3 pos;
		// プレイヤー最小座標
		Vector3 pos_min;
		// プレイヤー最大座標
		Vector3 pos_max;
	};
	// コンストラクタ
	Status();
	// コンストラクタ
	explicit Status(float maxHP);
	// デストラクタ
	~Status();
	// 初期化
	void init();
	// プレイヤーの座標データの登録
	void setPosData(Vector3 pos, Vector3 pos_min, Vector3 pos_max);
	// ダメージ計算
	void addDamage(float damage);
	// コンボ計算
	void addCombo();
	// 描画処理
	void draw() const;
public:
	// パラメータ
	Params mParams;
};