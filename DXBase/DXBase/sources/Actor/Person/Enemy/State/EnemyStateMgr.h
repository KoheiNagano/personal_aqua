#pragma once

#include "../../../State/Base/StateMgr.h"
#include "EnemyState_Enum.h"
#include "EnemyState.h"

class EnemyStateMgr : public StateMgr {
public:
	// コンストラクタ
	EnemyStateMgr();
	// コンストラクタ
	EnemyStateMgr(Vector3& pos, Matrix& mat);
	// 遷移可能な状態の追加
	void add(EnemyState_Enum id, const IStatePtr& state);
	// 状態の変更
	void change(Actor& actor, const EnemyState_Enum& id, const ActionType& type = ActionType::None);
	// 要素の取得
	IState::Component getComp();
	// 指定された状態かどうか
	bool isSpecifiedState(EnemyState_Enum id);
	// 指定されたタイプかどうか
	bool isSpecifiedType(ActionType type);
};
