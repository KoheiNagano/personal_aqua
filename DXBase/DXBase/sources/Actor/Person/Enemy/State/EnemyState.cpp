#include "EnemyState.h"

EnemyState::EnemyState(Vector3 & pos, Matrix & mat) :
	mPosition(pos), mRotation(mat), mAngle(0), mVelocity(Vector3::Zero) {

}

void EnemyState::change(const EnemyState_Enum & id, const ActionType & type){
	change_state(Component(static_cast<int>(id), type));
}

void EnemyState::key_input(){}

void EnemyState::pad_input(){}

void EnemyState::change_motion(Actor & actor, EnemyMotionID id){
	actor.setMotion(static_cast<int>(id));
}

void EnemyState::move(Actor & actor, float deltaTime, float speed){
}

bool EnemyState::awake_key(){
	return false;
}

bool EnemyState::awake_pad(){
	return false;
}
