#include "EnemyStateMgr.h"

#include "EnemyState_Path.h"

EnemyStateMgr::EnemyStateMgr(){}

EnemyStateMgr::EnemyStateMgr(Vector3 & pos, Matrix & mat){

}

void EnemyStateMgr::add(EnemyState_Enum id, const IStatePtr & state){
	// 状態の追加
	addState(static_cast<int>(id), state);
}

void EnemyStateMgr::change(Actor & actor, const EnemyState_Enum & id, const ActionType & type){
	changeState(actor, IState::Component(static_cast<int>(id), type));
}

IState::Component EnemyStateMgr::getComp(){
	// 状態の要素を返す
	return getComponent();
}

bool EnemyStateMgr::isSpecifiedState(EnemyState_Enum id){
	// 指定されたStateと同じだった場合true
	return mComponent.mID == static_cast<int>(id);
}

bool EnemyStateMgr::isSpecifiedType(ActionType type){
	// 指定されたTypeと同じだった場合true
	return mComponent.mType == type;
}
