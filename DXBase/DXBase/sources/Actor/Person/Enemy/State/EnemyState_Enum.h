#pragma once

enum class EnemyState_Enum {
	DAMMY,
	NORMAL,
	AWAKE,

	DAMAGE,

	GUARD,
	QUICK,

	IDLE,
	WALK,
	RUN,
	JUMP,

	ATTACK1,
	ATTACK2,
	ATTACK3,

	REACTION
};