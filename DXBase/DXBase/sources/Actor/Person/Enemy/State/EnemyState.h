#pragma once

#include "../../../State/Base/State.h"
#include "EnemyState_Enum.h"
#include "../../../../Math/Math.h"
#include "../EnemyMotionID.h"

// 各プレイヤーステートの基底クラス
class EnemyState : public State {
public:
	// コンストラクタ
	EnemyState(Vector3& pos, Matrix& mat);
	// ステートの変更
	void change(const EnemyState_Enum& id, const ActionType& type = ActionType::None);
	// キー入力処理
	virtual void key_input() override;
	// パッド入力処理
	virtual void pad_input() override;
protected:
	// モーションの切り替え
	void change_motion(Actor& actor, EnemyMotionID id);
	// 移動処理
	void move(Actor & actor, float deltaTime, float speed = 1.0f);
	// 覚醒条件(キー)
	bool awake_key();
	// 覚醒条件(パッド)
	bool awake_pad();
protected:
	// 座標
	Vector3& mPosition;
	// 行列
	Matrix&  mRotation;
	// 速度
	Vector3 mVelocity;
	// 回転角度
	float mAngle;
};
