#include "EnemyState_GuardEnd.h"

EnemyState_GuardEnd::EnemyState_GuardEnd(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}


void EnemyState_GuardEnd::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::GUARD_STANCE);
}

void EnemyState_GuardEnd::update(Actor & actor, const float & deltaTime){
}

void EnemyState_GuardEnd::end(){
}

void EnemyState_GuardEnd::key_input(){
}

void EnemyState_GuardEnd::pad_input(){
}
