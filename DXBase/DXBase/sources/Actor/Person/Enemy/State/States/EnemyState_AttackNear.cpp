#include "EnemyState_AttackNear.h"

EnemyState_AttackNear::EnemyState_AttackNear(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}

void EnemyState_AttackNear::unique_init(Actor & actor){
}

void EnemyState_AttackNear::update(Actor & actor, const float & deltaTime){
}

void EnemyState_AttackNear::end(){
}

void EnemyState_AttackNear::key_input(){
}

void EnemyState_AttackNear::pad_input(){
}
