#include "EnemyState_GuardStart.h"

EnemyState_GuardStart::EnemyState_GuardStart(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}

void EnemyState_GuardStart::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::GUARD_START);
}

void EnemyState_GuardStart::update(Actor & actor, const float & deltaTime){
}

void EnemyState_GuardStart::end(){
}

void EnemyState_GuardStart::key_input(){
}

void EnemyState_GuardStart::pad_input(){
}

