#include "EnemyState_Idle.h"

EnemyState_Idle::EnemyState_Idle(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}

void EnemyState_Idle::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::IDLE);
}

void EnemyState_Idle::update(Actor & actor, const float & deltaTime)
{
}

void EnemyState_Idle::end()
{
}

void EnemyState_Idle::key_input()
{
}

void EnemyState_Idle::pad_input()
{
}
