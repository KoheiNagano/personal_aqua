#include "EnemyState_GuardStance.h"

EnemyState_GuardStance::EnemyState_GuardStance(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}

void EnemyState_GuardStance::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::GUARD_STANCE);
}

void EnemyState_GuardStance::update(Actor & actor, const float & deltaTime){
}

void EnemyState_GuardStance::end(){
}

void EnemyState_GuardStance::key_input(){
}

void EnemyState_GuardStance::pad_input(){
}

