#include "EnemyState_Move.h"

EnemyState_Move::EnemyState_Move(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}

void EnemyState_Move::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::RUN);
}

void EnemyState_Move::update(Actor & actor, const float & deltaTime){
}

void EnemyState_Move::end(){
}

void EnemyState_Move::key_input(){
}

void EnemyState_Move::pad_input(){
}
