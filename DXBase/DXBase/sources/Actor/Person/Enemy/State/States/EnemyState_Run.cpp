#include "EnemyState_Run.h"

EnemyState_Run::EnemyState_Run(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}

void EnemyState_Run::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::RUN);
}

void EnemyState_Run::update(Actor & actor, const float & deltaTime)
{
}

void EnemyState_Run::end()
{
}

void EnemyState_Run::key_input()
{
}

void EnemyState_Run::pad_input()
{
}
