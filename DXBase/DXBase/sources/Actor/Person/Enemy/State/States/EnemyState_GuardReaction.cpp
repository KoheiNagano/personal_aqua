#include "EnemyState_GuardReaction.h"

EnemyState_GuardReaction::EnemyState_GuardReaction(Vector3 & pos, Matrix & mat) : EnemyState(pos, mat) {}


void EnemyState_GuardReaction::unique_init(Actor & actor){
	change_motion(actor, EnemyMotionID::GUARD_REACTION);
}

void EnemyState_GuardReaction::update(Actor & actor, const float & deltaTime){
}

void EnemyState_GuardReaction::end(){
}

void EnemyState_GuardReaction::key_input(){
}

void EnemyState_GuardReaction::pad_input(){
}
