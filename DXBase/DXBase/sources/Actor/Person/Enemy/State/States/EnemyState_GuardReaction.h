#pragma once

#include "../EnemyState.h"

// 待機(通常)状態
class EnemyState_GuardReaction : public EnemyState {
public:
	// コンストラクタ
	EnemyState_GuardReaction(Vector3& pos, Matrix& mat);
	// 各状態固有の初期化
	virtual void unique_init(Actor & actor) override;
	// 更新処理
	virtual void update(Actor & actor, const float& deltaTime) override;
	// 終了時の処理
	virtual void end() override;
	// キー入力処理
	virtual void key_input() override;
	// パッド入力処理
	virtual void pad_input() override;
};
