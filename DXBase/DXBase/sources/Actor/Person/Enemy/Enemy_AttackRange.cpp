#include "Enemy_AttackRange.h"
#include "../../Body/BoundingSphere.h"

// コンストラクタ
Enemy_AttackRange::Enemy_AttackRange(IWorld * world, Matrix& bone) :
	Actor(world, "Enemy_AttackRange", bone.Translation(), std::make_shared<BoundingSphere>(Vector3::Zero, 2.0f)),
	mLifeTimer(0), mWeaponMatrix(bone) {}

// 更新処理
void Enemy_AttackRange::onUpdate(float deltaTime){
	// 武器の行列に合わせて座標を設定
	mPosition = mWeaponMatrix.Translation() + mWeaponMatrix.Backward() * 5;
	// 時間経過で消滅
	mLifeTimer += deltaTime;
	if (mLifeTimer >= LIFE_TIME) dead();
}

// 衝突判定
void Enemy_AttackRange::onCollide(Actor & other){
	dead();
}
