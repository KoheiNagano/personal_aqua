#pragma once

#include "../../Base/Actor.h"
#include "../../../World/IWorld.h"
#include "../../../Math/Math.h"

// アニメーションの列挙
enum {
	IDLE = 0,
	RUN = 1,
	ATTACK = 2,
	GUARD_START = 3,
	GUARD_STANCE = 4,
	GUARD_END = 5,
	GUARD_REACTION = 6,
	DAMAGE = 7,
	DYING_BACK = 8,
	DYING_FRONT = 9,
};

// 敵
class Enemy : public Actor {
public:
	// 状態の列挙
	enum class State {
		INIT,
		IDLE,
		MOVE,
		CHASE,
		ATTACK,
		DAMAGE,
		DEAD
	};
	// コンストラクタ
	Enemy(IWorld* world, const Vector3&  position);
	// デストラクタ
	~Enemy();
	// 更新処理
	virtual void onUpdate(float deltaTime) override;
	// 描画処理
	virtual void onDraw() const override;
	// 衝突処理
	virtual void onCollide(Actor& actor) override;
private:
	// 状態の更新
	void updateState(float deltaTime);
	// 状態の変更
	void changeState(State state, unsigned int motion);
	// 初期化
	void init();
	// 待機処理
	void idle(float deltaTime);
	// 移動処理
	void move(float deltaTime);
	// 追いかけ処理
	void chase(float deltaTime);
	// 攻撃処理
	void attack(float deltaTime);
	// ダメージ処理
	void damage(float deltaTime);
	// 次の座標を設定
	void setPos();
private:
	// タイマー
	float mTime;
	// 現在の状態
	State mState;
	// 状態のタイマー
	float mStateTimer;
	// 攻撃対象
	Vector3 mTarget;
	// 初期座標
	Vector3 mStart;
	// 攻撃座標
	Matrix mWeaponMatrix;
	// 剣のモデルハンドル
	unsigned int mSwordHandle;
	// 盾のモデルハンドル
	unsigned int mShieldHandle;
	// 剣の設置場所
	unsigned int mSwordAttachFrame;
	// 盾の設置場所
	unsigned int mShieldAttachFrame;
};