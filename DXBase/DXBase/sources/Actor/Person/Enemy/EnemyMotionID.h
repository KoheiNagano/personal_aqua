#pragma once

// エネミーモーションの列挙
enum class EnemyMotionID {
	IDLE = 0,
	RUN = 1,
	ATTACK = 2,
	GUARD_START = 3,
	GUARD_STANCE = 4,
	GUARD_END = 5,
	GUARD_REACTION = 6,
	DAMAGE = 7,
	DYING_BACK = 8,
	DYING_FRONT = 9,
};
