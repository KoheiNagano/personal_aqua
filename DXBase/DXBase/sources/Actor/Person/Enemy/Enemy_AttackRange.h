#pragma once

#include "../../Base/Actor.h"

// �G�U������
class Enemy_AttackRange : public Actor {
public:
	// �R���X�g���N�^
	Enemy_AttackRange(IWorld* world, Matrix& bone);
private:
	// �X�V
	virtual void onUpdate(float deltaTime) override;
	// �Փ˔���
	virtual void onCollide(Actor& other) override;
private:
	// ����̍s��
	Matrix& mWeaponMatrix;
	// ����̑��݂��鎞��
	const float LIFE_TIME = 1.0f;
	// ����
	float mLifeTimer;
};
