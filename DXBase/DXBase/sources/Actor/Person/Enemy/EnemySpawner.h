#pragma once

#include "../../Base/Actor.h"

//エネミー生成クラス
class EnemySpawner : public Actor {
public:
	// コンストラクタ
	EnemySpawner(IWorld* world, const std::vector<std::vector<int>>& stage);
	// デストラクタ	
	~EnemySpawner();
private:
	// エネミー生成
	void spawn(const std::vector<std::vector<int>>& stage);
};

