#include "EnemySpawner.h"

#include "../../../World/IWorld.h"
#include "../../Base/ActorGroup.h"
#include "Enemy.h"

// コンストラクタ
EnemySpawner::EnemySpawner(IWorld * world, const std::vector<std::vector<int>>& stage) :
	Actor(world, "EnemySpawner"){
	// エネミー生成
	spawn(stage);
}

// デストラクタ	
EnemySpawner::~EnemySpawner(){}

// エネミー生成
void EnemySpawner::spawn(const std::vector<std::vector<int>>& stage){
	// データを受け取り指定数生成
	for (int r = 0; r < stage.size(); ++r) {
		Vector3 pos = Vector3(stage[r][0], stage[r][1], stage[r][2]);
		mWorld->addActor(ActorGroup::ENEMY, std::make_shared<Enemy>(mWorld, pos));
	}
}
