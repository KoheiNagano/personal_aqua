#include "Enemy.h"

#include "../../../ResourceLoader/ResourceLoader.h"
#include "../../Base/ActorGroup.h"
#include "Enemy_AttackRange.h"
#include "EnemyMotionID.h"
#include "../../Body/BoundingCapsule.h"

#include "../../../Define.h"
#include "../../../Scene/Base/SceneData.h"
#include <algorithm>

// コンストラクタ
Enemy::Enemy(IWorld * world, const Vector3 & position) :
	Actor(world, "Enemy", position, std::make_shared<BoundingCapsule>(Vector3::Up * PERSON_CENTER_HEIGHT, Matrix::Identity, PERSON_CENTER_HEIGHT, PERSON_RADIUS)),
	mTime(0), mState(State::IDLE), mStateTimer(0), mTarget(Vector3::Zero){
	// モデルの読み込み
	mModelHandle = MV1DuplicateModel(ResourceLoader::GetInstance().getModelHandle(ModelID::ENEMY));
	mAnimation = Animation(mModelHandle);

	//初期状態を正面に設定
	mRotation *= Matrix::CreateFromAxisAngle(mRotation.Up(), 0);
	mRotation.NormalizeRotationMatrix();

	mAlpha = 255;
	// 初期状態の設定
	changeState(State::DAMAGE, IDLE);

	// 初期座標を保存
	mStart = mPosition;

	// 剣のモデルハンドル
	mSwordHandle = MV1DuplicateModel(ResourceLoader::GetInstance().getModelHandle(ModelID::ENEMY_SWORD));
	// 盾のモデルハンドル
	mShieldHandle = MV1DuplicateModel(ResourceLoader::GetInstance().getModelHandle(ModelID::ENEMY_SHIELD));
	
	// 剣の設置場所
	mSwordAttachFrame = MV1SearchFrame(mModelHandle, "R_HandThumb3");
	// 盾の設置場所
	mShieldAttachFrame = MV1SearchFrame(mModelHandle, "L_Hand");
}

// デストラクタ
Enemy::~Enemy(){
}

// 更新処理
void Enemy::onUpdate(float deltaTime){
	// 重力を加算
	mPosition += Vector3::Down * GRAVITY;

	// 状態の更新
	updateState(deltaTime);

	// アニメーションの更新
	mAnimation.changeAnim(mMotionID, mMotionSpeed);
	mAnimation.update(deltaTime);

	// 地形との衝突判定
	field();

	// 行動制限
	mPosition = Vector3::Clamp(mPosition, mWorld->getSceneData()->getCurData().status.mParams.pos_min, mWorld->getSceneData()->getCurData().status.mParams.pos_max);

	// 変換行列をモデルに適応
	MV1SetMatrix(mModelHandle, Matrix::MatrixToMATRIX(getPose()));
}

// 描画処理
void Enemy::onDraw() const{
	SetDrawAlphaTest(DX_CMP_GREATER, 0);
	MV1SetOpacityRate(mModelHandle, mAlpha / 255.0f);
	// プレイヤーモデルの描画
	MV1DrawModel(mModelHandle);

	// 武器モデルの描画
	MATRIX weaponFrameMatrix = MV1GetFrameLocalWorldMatrix(mModelHandle, mSwordAttachFrame);
	MV1SetMatrix(mSwordHandle, weaponFrameMatrix);
	MV1DrawModel(mSwordHandle);

	// 盾モデルの描画
	MATRIX shieldFrameMatrix = MV1GetFrameLocalWorldMatrix(mModelHandle, mShieldAttachFrame);
	MV1SetMatrix(mShieldHandle, shieldFrameMatrix);
	MV1DrawModel(mShieldHandle);
}

// 衝突処理
void Enemy::onCollide(Actor & actor){
	if (actor.getName() == "Player_AttackRange") {
		mWorld->getSceneData()->getCurData().status.addDamage(PLAYER_ABSORB_BLOOD_AMOUNT);
		changeState(State::DAMAGE, DYING_BACK);
		mBody->active(false);
	}
}

// 状態の更新
void Enemy::updateState(float deltaTime){
	switch (mState) {
	case State::INIT:init(); break;
	case State::IDLE:idle(deltaTime); break;
	case State::MOVE:move(deltaTime); break;
	case State::CHASE:chase(deltaTime); break;
	case State::ATTACK:attack(deltaTime); break;
	case State::DAMAGE:damage(deltaTime); break;
	};
	mStateTimer += deltaTime;
	mTime += deltaTime;
}

// 状態の変更
void Enemy::changeState(State state, unsigned int motion){
	// モーションの変更
	mMotionID = motion;
	// ステートの変更
	mState = state;
	// タイマーの初期化
	mStateTimer = 0.0f;
}

// 初期化
void Enemy::init() {
	mAlpha = std::min<float>(mAlpha + mStateTimer * 2, 255.0f);

	if (mAnimation.isAnimEnd()) {
		changeState(State::MOVE, IDLE);
	}
}

// 待機処理
void Enemy::idle(float deltaTime){

	if (mAnimation.isAnimEnd())
	{
		mBody->active(true);
		changeState(State::MOVE, RUN);
	}
}

// 移動処理
void Enemy::move(float deltaTime){
	// プレイヤーが見つからない場合return
	auto player = mWorld->findActor("Player");
	if (player == nullptr) return;

	// プレイヤーとの距離を計算
	mTarget = player->position();
	Vector3 to_target = mTarget - mPosition;
	
	// 一定の距離以内になったら追いかけ開始
	if (to_target.Length() <= 100)changeState(State::CHASE, RUN);
}

// 追いかけ処理
void Enemy::chase(float deltaTime){
	// プレイヤーが見つからない場合return
	auto player = mWorld->findActor("Player");
	if (player == nullptr) return;

	// プレイヤーに向かって移動
	mTarget = player->position();

	mRotation.NormalizeRotationMatrix();

	// プレイヤーへのベクトル
	Vector3 to_target = mTarget - mPosition;
	// 外積内積を使い、プレイヤーとのなす角を計算
	Vector3 forward_cross_target = Vector3::Cross(mRotation.Backward(), to_target);
	float up_dot_cross = Vector3::Dot(mRotation.Up(), forward_cross_target);
	float angle = Vector3::Angle(mRotation.Backward(), to_target);

	// プレイヤーの方を向きながら追いかける
	float yaw = std::min<float>(2.0f, angle);
	yaw = (up_dot_cross >= 0.0f) ? yaw : -yaw;
	mRotation *= Matrix::CreateFromAxisAngle(mRotation.Up(), yaw);
	mPosition += mRotation.Backward() * 0.5f;

	// 攻撃圏内に入ったら攻撃開始
	if (to_target.Length() <= 20) {
		// 攻撃判定の生成
		mWorld->addActor(ActorGroup::ENEMY_ATTACK_RANGE, std::make_shared<Enemy_AttackRange>(mWorld, mWeaponMatrix));
		// 状態の遷移
		changeState(State::ATTACK, ATTACK);
	}
	// ある程度離れたら待機状態に戻る
	if (to_target.Length() > 100)changeState(State::MOVE, IDLE);
}

// 攻撃処理
void Enemy::attack(float deltaTime){
	// 武器モデルの変換行列を代入
	mWeaponMatrix = Matrix::MATRIXToMatrix(MV1GetMatrix(mSwordHandle));

	//一回攻撃したら待機状態に戻る
	if (mAnimation.isAnimEnd()) {
		changeState(State::IDLE, IDLE);
	}
}

// ダメージ処理
void Enemy::damage(float deltaTime){
	mAlpha = std::max<float>(mAlpha - mStateTimer, 0);
	if (mAnimation.isAnimEnd()) {
		setPos();
		changeState(State::INIT, IDLE);
	}
}

// 次の座標を設定
void Enemy::setPos(){
	// スタート地点に戻る
	mPosition = mStart;
}




