#pragma once

#include "../../Math/Math.h"

class Collision {
public:
	// �����m�̏Փ˔���
	static bool Sphere_Sphere(const Vector3& p1, const float& radius1, const Vector3& p2, const float& radius2);
	
	// �J�v�Z�����m�̏Փ˔���
	static bool Capsule_Capsule(const Vector3 p1[], const float& radius1, const Vector3 p2[], const float& radius2);
	static bool Capsule_Capsule(const Vector3& pos1, const Matrix& mat1, const float & len1, const float& radius1, const Vector3& pos2, const Matrix& mat2, const float & len2, const float& radius2);

	// �������m�̏Փ˔���
	static bool Segment_Segment(const Vector3 p1[], const Vector3 p2[]);
	static bool Segment_Segment(const Vector3& pos1, const Matrix& mat1, const float & length1, const Vector3& pos2, const Matrix& mat2, const float & length2);

	// ���ƃJ�v�Z���̏Փ˔���
	static bool Sphere_Capsule(const Vector3& sphere, const float& sphere_r, const Vector3 capsule[], const float& capsule_r);
	static bool Sphere_Capsule(const Vector3& sphere, const float& sphere_r, const Vector3& cap_pos, const Matrix& cap_mat, const float & cap_len, const float& cap_radius);

	// ���Ɛ����̏Փ˔���
	static bool Sphere_Segment(const Vector3& sphere, const float& sphere_r, const Vector3 segment[]);
	static bool Sphere_Segment(const Vector3& sphere, const float& sphere_r, const Vector3& seg_pos, const Matrix& seg_mat, const float & seg_len);

	// �J�v�Z���Ɛ����̏Փ˔���
	static bool Capsule_Segment(const Vector3 capsule[], const float& capsule_r, const Vector3 segment[]);
	static bool Capsule_Segment(const Vector3& seg_pos, const Matrix& seg_mat, const float & seg_len, const Vector3& cap_pos, const Matrix& cap_mat, const float & cap_len, const float& cap_radius);
};