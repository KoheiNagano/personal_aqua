#include "Collision.h"

#include <algorithm>

// �����m�̏Փ˔���
bool Collision::Sphere_Sphere(const Vector3& p1, const float& radius1, const Vector3& p2, const float& radius2){
	return Vector3::Distance(p1, p2) <= radius1 + radius2;
}

// �J�v�Z�����m�̏Փ˔���
bool Collision::Capsule_Capsule(const Vector3 p1[], const float& radius1, const Vector3 p2[], const float& radius2) {
	VECTOR p1_start = Vector3::Vector3ToVECTOR(p1[0]);
	VECTOR p1_end = Vector3::Vector3ToVECTOR(p1[1]);
	VECTOR p2_start = Vector3::Vector3ToVECTOR(p2[0]);
	VECTOR p2_end = Vector3::Vector3ToVECTOR(p2[1]);

	//if (HitCheck_Capsule_Capsule(p1_start, p1_end, radius1, p2_start, p2_end, radius2) == TRUE)return true;
	//else return false;

	return HitCheck_Capsule_Capsule(p1_start, p1_end, radius1, p2_start, p2_end, radius2);
}

// �J�v�Z�����m�̏Փ˔���
bool Collision::Capsule_Capsule(const Vector3 & pos1, const Matrix & mat1, const float & len1, const float & radius1, const Vector3 & pos2, const Matrix & mat2, const float & len2, const float & radius2)
{
	Vector3 p1[2], p2[2];
	p1[0] = pos1 + Vector3::Up * len1 * mat1;
	p1[1] = pos1 + Vector3::Down * len1 * mat1;
	p2[0] = pos2 + Vector3::Up * len2 * mat2;
	p2[1] = pos2 + Vector3::Down * len2 * mat2;

	return Capsule_Capsule(p1, radius1, p2, radius2);
}

// �������m�̏Փ˔���
bool Collision::Segment_Segment(const Vector3 p1[], const Vector3 p2[]){
	VECTOR p1_start = Vector3::Vector3ToVECTOR(p1[0]);
	VECTOR p1_end = Vector3::Vector3ToVECTOR(p1[1]);
	VECTOR p2_start = Vector3::Vector3ToVECTOR(p2[0]);
	VECTOR p2_end = Vector3::Vector3ToVECTOR(p2[1]);

	return Segment_Segment_MinLength_Square(p1_start, p1_end, p2_start, p2_end) < 0;
}

// �������m�̏Փ˔���
bool Collision::Segment_Segment(const Vector3 & pos1, const Matrix & mat1, const float & length1, const Vector3 & pos2, const Matrix & mat2, const float & length2){
	Vector3 p1[2], p2[2];
	p1[0] = pos1 + Vector3::Up * length1 * mat1;
	p1[1] = pos1 + Vector3::Down * length1 * mat1;
	p2[0] = pos2 + Vector3::Up * length2 * mat2;
	p2[1] = pos2 + Vector3::Down * length2 * mat2;
	return Segment_Segment(p1, p2);
}

// ���ƃJ�v�Z���̏Փ˔���
bool Collision::Sphere_Capsule(const Vector3& sphere, const float& sphere_r, const Vector3 capsule[], const float& capsule_r){
	VECTOR sphere_pos = Vector3::Vector3ToVECTOR(sphere);
	VECTOR capsule_start = Vector3::Vector3ToVECTOR(capsule[0]);
	VECTOR capsule_end = Vector3::Vector3ToVECTOR(capsule[1]);

	return Segment_Point_MinLength_Square(capsule_start, capsule_end, sphere_pos) < std::powf(sphere_r + capsule_r, 2);
}

// ���ƃJ�v�Z���̏Փ˔���
bool Collision::Sphere_Capsule(const Vector3 & sphere, const float & sphere_r, const Vector3 & cap_pos, const Matrix & cap_mat, const float & cap_len, const float & cap_radius)
{
	Vector3 p[2];
	p[0] = cap_pos + Vector3::Up * cap_len * cap_mat;
	p[1] = cap_pos + Vector3::Down * cap_len * cap_mat;

	return Sphere_Capsule(sphere, sphere_r, p, cap_radius);
}

// ���Ɛ����̏Փ˔���
bool Collision::Sphere_Segment(const Vector3& sphere, const float& sphere_r, const Vector3 & seg_pos, const Matrix & seg_mat, const float & seg_len){
	Vector3 p[2];
	p[0] = seg_pos + Vector3::Up * seg_len * seg_mat;
	p[1] = seg_pos + Vector3::Down * seg_len * seg_mat;

	return Sphere_Segment(sphere, sphere_r, p);
}

// ���Ɛ����̏Փ˔���
bool Collision::Sphere_Segment(const Vector3& pos, const float& radius, const Vector3 segment[]){
	VECTOR segment_start = Vector3::Vector3ToVECTOR(segment[0]);
	VECTOR segment_end   = Vector3::Vector3ToVECTOR(segment[1]);
	VECTOR sphere_pos = Vector3::Vector3ToVECTOR(pos);
	
	return HitCheck_Line_Sphere(segment_start, segment_end, sphere_pos, radius);
}

// �J�v�Z���Ɛ����̏Փ˔���
bool Collision::Capsule_Segment(const Vector3 capsule[], const float& capsule_r, const Vector3 segment[]){
	VECTOR p1_start = Vector3::Vector3ToVECTOR(capsule[0]);
	VECTOR p1_end = Vector3::Vector3ToVECTOR(capsule[1]);
	VECTOR p2_start = Vector3::Vector3ToVECTOR(segment[0]);
	VECTOR p2_end = Vector3::Vector3ToVECTOR(segment[1]);

	return Segment_Segment_MinLength_Square(p1_start, p1_end, p2_start, p2_end) < std::powf(capsule_r, 2);
}

// �J�v�Z���Ɛ����̏Փ˔���
bool Collision::Capsule_Segment(const Vector3 & seg_pos, const Matrix & seg_mat, const float & seg_len, const Vector3 & cap_pos, const Matrix & cap_mat, const float & cap_len, const float & cap_radius){
	Vector3 p1[2], p2[2];
	p1[0] = cap_pos + Vector3::Up * cap_len * cap_mat;
	p1[1] = cap_pos + Vector3::Down * cap_len * cap_mat;
	p2[0] = seg_pos + Vector3::Up * seg_len * seg_mat;
	p2[1] = seg_pos + Vector3::Down * seg_len * seg_mat;

	return Capsule_Segment(p1, cap_radius, p2);
}
