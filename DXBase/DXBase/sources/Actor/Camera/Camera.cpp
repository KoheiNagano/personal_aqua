#include "Camera.h"

#include "../../Define.h"

#include "../../Input/InputMgr.h"
#include "../../World/IWorld.h"

// コンストラクタ
Camera::Camera(IWorld* world, const Vector3& position) :
	Actor(world, "Camera", position), mAngle(CAMERA_INIT_ANGLE) {

	// 注視点の設定(バネ補正付き)
	mTarget = mPosition + Vector3(0.0f, 20.0f, 0.0f);
	
	// 回転角度の設定
	// 水平回転
	Matrix rot_hor = Matrix::CreateFromAxisAngle(Vector3::Up, mAngle.x);
	// 平行回転
	Matrix rot_ver = Matrix::CreateFromAxisAngle(getPose().Right(), mAngle.z);
	// 回転行列を合成、代入
	mRotation = rot_hor * rot_ver;
	// 行列の正規化
	mRotation.NormalizeRotationMatrix();

	// 座標の更新(バネ補正付き)
	mPosition += Vector3::Up * 20 + Vector3::Forward * 50 * mRotation;

	// カメラの情報をライブラリのカメラに反映させる
	SetCameraPositionAndTarget_UpVecY(Vector3::Vector3ToVECTOR(mPosition), Vector3::Vector3ToVECTOR(mTarget));
}

// 更新
void Camera::onUpdate(float deltaTime) {
	(void)deltaTime;
	// プレーヤを検索
	auto player = mWorld->findActor("Player");
	// プレーヤーがいない場合はreturn
	if (player == nullptr) return;

	// 入力による回転角度の指定
	mAngle += InputMgr::GetInstance().AnalogPadVectorR() * CAMERA_INPUT_SPEED * deltaTime * 60;
	// 回転角度の制限
	mAngle.z = MathHelper::Clamp(mAngle.z, CAMERA_ANGLE_MIN, CAMERA_ANGLE_MAX);

	// 注視点の設定(バネ補正付き)
	Vector3::Spring(mTarget, player->position() + Vector3(0.0f, 20.0f, 0.0f) + player->rotation().Backward() * 10, mTargetV, 0.5f, 0.2f, 1.0f);
	
	// 回転角度の設定
	// 水平回転
	Matrix rot_hor = Matrix::CreateFromAxisAngle(Vector3::Up, mAngle.x);
	// 平行回転
	Matrix rot_ver = Matrix::CreateFromAxisAngle(getPose().Right(), mAngle.z);
	// 回転行列を合成、代入
	mRotation = rot_hor * rot_ver;
	// 行列の正規化
	mRotation.NormalizeRotationMatrix();

	// 座標の更新(バネ補正付き)
	Vector3::Spring(mPosition, Vector3::Forward * 50 * mRotation + player->position() + Vector3::Up * 20, mPosV, 0.5f, 0.2f, 1.0f);
}

// 描画
void Camera::onDraw() const {
	// カメラの描画範囲を設定
	SetCameraNearFar(CAMERA_NEAR, CAMERA_FAR);
	// カメラの情報をライブラリのカメラに反映させる
	SetCameraPositionAndTarget_UpVecY(Vector3::Vector3ToVECTOR(mPosition), Vector3::Vector3ToVECTOR(mTarget));
}

