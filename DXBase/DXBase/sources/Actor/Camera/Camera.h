#pragma once

#include "../Base/Actor.h"

// カメラクラス
class Camera : public Actor {
public:
	// コンストラクタ
	explicit Camera(IWorld* world, const Vector3& position);
private:
	// 更新
	virtual void onUpdate(float deltaTime) override;
	// 描画
	virtual void onDraw() const override;
private:
	// 座標の補正速度
	Vector3 mPosV;
	// 注視点
	Vector3 mTarget;
	// 注視点の補正速度
	Vector3 mTargetV;
	// 回転角度
	Vector3 mAngle;
};



