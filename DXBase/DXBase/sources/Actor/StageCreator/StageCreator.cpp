#include "StageCreator.h"

#include "../../Define.h"
#include "../../World/IWorld.h"
#include "../../Actor/Person/Player/Player.h"
#include "../../Actor/Person/Enemy/EnemySpawner.h"
#include "../../Actor/Base/ActorGroup.h"

#include "../../Scene/Base/SceneData.h"
#include "../../Stage/StageComponent.h"
#include "../../Stage/StageLoader.h"

// コンストラクタ
StageCreator::StageCreator(IWorld * world, const SceneDataPtr& data) : Actor(world, "StageCreator") {
	// ステージ要素を生成
	create(data);
}

StageCreator::~StageCreator(){}

// ステージ要素を生成
void StageCreator::create(const SceneDataPtr& data){
	// ステージ構成ファイルのロード
	StageLoader stage = StageLoader(data->getCurData().path + std::to_string(data->mSceneCount) + ".csv");
	// ファイルから読み込んだプレイヤーのデータを取得
	std::vector<int> pData = stage.playerData();
	Vector3 pos = Vector3(pData[PLAYER_START_POS_X], pData[PLAYER_START_POS_Y], pData[PLAYER_START_POS_Z]);
	Vector3 pos_min = Vector3(pData[PLAYER_MIN_POS_X], pData[PLAYER_MIN_POS_Y], pData[PLAYER_MIN_POS_Z]);
	Vector3 pos_max = Vector3(pData[PLAYER_MAX_POS_X], pData[PLAYER_MAX_POS_Y], pData[PLAYER_MAX_POS_Z]);
	// ステージのデータとして格納
	mWorld->getSceneData()->getCurData().status.setPosData(pos, pos_min, pos_max);

	// プレイヤーの生成
	mWorld->addActor(ActorGroup::PLAYER, std::make_shared<Player>(mWorld, mWorld->getSceneData()->getCurData().status.mParams.pos));
	// 敵生成機の生成
	mWorld->addActor(ActorGroup::Enemy_Spawner, std::make_shared<EnemySpawner>(mWorld, stage.getData(StageComponent::Enemy)));
}

