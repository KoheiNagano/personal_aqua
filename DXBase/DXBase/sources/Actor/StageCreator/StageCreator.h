#pragma once

#include "../Base/Actor.h"
#include "../../Scene/Base/SceneDataPtr.h"

//ステージ生成クラス
class StageCreator : public Actor {
public:
	// コンストラクタ
	StageCreator(IWorld* world, const SceneDataPtr& data);
	// デストラクタ	
	~StageCreator();
private:
	// ステージ要素を生成
	void create(const SceneDataPtr& data);
};
