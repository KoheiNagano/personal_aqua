#pragma once

#include "IBody.h"

class DammyBody : public IBody{
public:
	DammyBody() {}
	virtual bool isCollide(const IBody& other, HitInfo& hitinfo) const override { return false; }
	virtual bool intersects(const BoundingSphere& other, HitInfo& hitinfo) const override { return false; }
	virtual bool intersects(const BoundingCapsule& other, HitInfo& hitinfo) const override { return false; }
	virtual bool intersects(const BoundingSegment& other, HitInfo& hitinfo) const override { return false; }
	virtual bool intersects(const BoundingBox& other, HitInfo& hitinfo) const override { return false; }
	virtual bool intersects(const Model& other, HitInfo& hitinfo) const override { return false; }
	virtual IBodyPtr translate(const Vector3& matrix) const override { return std::make_shared<DammyBody>(); }
	virtual IBodyPtr transform(const Matrix& matrix) const override { return std::make_shared<DammyBody>(); }
	virtual void active(bool active) override {  }
	virtual void draw() const override{}
	virtual ShapeType type() const override { return ShapeType::None; }
	virtual Vector3 center() const override { return Vector3::Zero; }
	virtual Matrix matrix() const override { return Matrix::Identity; }
	virtual float radius() const override { return 0.0f; }
	virtual float length() const override { return 0.0f; }
	virtual float height() const override { return 0.0f; }
	virtual float width() const override { return 0.0f; }

};
