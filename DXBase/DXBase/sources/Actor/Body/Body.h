#pragma once

#include "IBody.h"
#include "../../Math/Math.h"

class Body : public IBody {
public:
	Body(const ShapeType& type = ShapeType::None, const Vector3& center = Vector3::Zero, const Matrix& mat = Matrix::Identity, const float& radius = 0, const float& length = 0, const float& height = 0, const float& width = 0, const bool& enabled = true) :
		mType(type), mCenter(center), mMatrix(mat), mRadius(radius), mLength(length), mHeight(height), mWidth(width), mEnabled(enabled) {}
	virtual ~Body(){}
	virtual void active(bool active) override { mEnabled = active; }
	virtual ShapeType type() const override { return mType; }
	virtual Vector3 center() const override { return mCenter; }
	virtual Matrix matrix() const override { return mMatrix; }
	virtual float radius() const override { return mRadius; }
	virtual float length() const override { return mLength; }
	virtual float height() const override { return mHeight; }
	virtual float width() const override { return mWidth; }
public:
	ShapeType mType;
	Vector3 mCenter;
	Matrix mMatrix;
	float mRadius;
	float mLength;
	float mHeight;
	float mWidth;
	bool mEnabled;
};

