#pragma once

enum class ShapeType {
	None,
	Sphere,
	Capsule,
	Segment,
	Count
};