#include "Light.h"

// コンストラクタ
Light::Light(IWorld* world, const Vector3& position) :
	Actor(world, "Light", position) {
	SetUseLighting(TRUE);
	ChangeLightTypeDir(VGet(1.0f, -1.0f, 1.0f));
}

// 描画
void Light::onDraw() const {}
