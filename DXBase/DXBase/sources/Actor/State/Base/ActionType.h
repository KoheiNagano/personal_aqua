#pragma once

// ステートのタイプの列挙
enum class ActionType {
	None,	// 無し
	Right,	// 右
	Left	// 左
};
