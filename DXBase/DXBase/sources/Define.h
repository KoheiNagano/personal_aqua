#pragma once
#include "Math/Math.h"

#include <string>

// ウインドウ系統パラメータ
///スクリーンのサイズ倍率
///const static float		SCREEN_RATE = 200;
///スクリーンのサイズ
const static Vector2	SCREEN_SIZE = Vector2(1920, 1080);
///スクリーンのアスペクト比
const static float		SCREEN_ASPECT = SCREEN_SIZE.x / SCREEN_SIZE.y;
///ウインドウの表示倍率(おそらくデバック時のみ)
const static float		WINDOW_RATE = 1.0f;
///ウインドウモードにするかしないか(TRUE : FALSE)
const static int		WINDOW_MODE = FALSE;

// フィールド系統パラメータ
///フィールドの座標
const static Vector3	FIELD_POS = Vector3::Up * 10 + Vector3::Right * 210 + Vector3::Forward * 230;
///フィールドのサイズ
const static float		FIELD_SIZE = 1000.0f;
///フィールドの拡大率
const static float		FIELD_SCALE = 10.0f;
///ステージのパス
const static std::string	STAGE1_PATH = "./resources/streaming/stage0";
///ステージ数
const static int		STAGE_NUM_MAX = 3;

// 数値系パラメータ
static const float		HALF = 0.5f;

// カメラ系統パラメータ
///カメラの初期角度
static const Vector3	CAMERA_INIT_ANGLE = Vector3(180.0f, 0.0f, 30.0f);
///カメラの入力速度
static const float		CAMERA_INPUT_SPEED = 3.0f;
///カメラの最大回転角度限界
static const float		CAMERA_ANGLE_MAX = 20.0f;
///カメラの最小回転角度限界
static const float		CAMERA_ANGLE_MIN = -20.0f;
///カメラの最大描画範囲
static const float		CAMERA_NEAR = 2.0f;
///カメラの最小描画範囲
static const float		CAMERA_FAR  = 50000.0f;

// アクター系統パラメータ
///アクターの重力
static const float		GRAVITY = 9.8f;
///人型の高さ
static const float		PERSON_HEIGHT = 17.0f;
///人型の中心の高さ(高さの半分)
static const float		PERSON_CENTER_HEIGHT = PERSON_HEIGHT * HALF;
///人型の半径(中心の高さの半分)
static const float		PERSON_RADIUS = PERSON_CENTER_HEIGHT * HALF;

// 衝突判定系統パラメータ
/////処理するコリジョンポリゴンの最大数
//static const int		MAX_HITCOLL = 2048;
/////壁押し出し処理の最大試行回数
//static const int		HIT_TRYNUM = 1000;
///一度の壁押し出し処理でスライドさせる距離
static const float		HIT_SLIDE_LENGTH = 10.0f;
///乗り上げることのできる斜面の限界
static const float		HIT_SLOPE_LIMIT = 0.5f;

// プレイヤー系統パラメータ
///プレイヤーの初期座標
const static Vector3	START_POS = Vector3(1600, 50, 1000);
///プレイヤーのジャンプ力
const static float		PLAYER_JUMP_POWER = 15.0f;
///プレイヤーのジャンプ減衰力
const static float		PLAYER_JUMP_DUMP = 0.4f;
///プレイヤーの覚醒時の血液消費量
const static float		PLAYER_AWAKE_BLOOD_USE = 0.1f;
///プレイヤーの吸収血液消費量
const static float		PLAYER_ABSORB_BLOOD_AMOUNT = -0.2f;
///プレイヤーのデータのインデックス(初期X座標)
const static int		PLAYER_START_POS_X = 0;
///プレイヤーのデータのインデックス(初期Y座標)
const static int		PLAYER_START_POS_Y = 1;
///プレイヤーのデータのインデックス(初期Z座標)
const static int		PLAYER_START_POS_Z = 2;
///プレイヤーのデータのインデックス(最小X座標)
const static int		PLAYER_MIN_POS_X = 3;
///プレイヤーのデータのインデックス(最小Y座標)
const static int		PLAYER_MIN_POS_Y = 4;
///プレイヤーのデータのインデックス(最小Z座標)
const static int		PLAYER_MIN_POS_Z = 5;
///プレイヤーのデータのインデックス(最大X座標)
const static int		PLAYER_MAX_POS_X = 6;
///プレイヤーのデータのインデックス(最大Y座標)
const static int		PLAYER_MAX_POS_Y = 7;
///プレイヤーのデータのインデックス(最大Z座標)
const static int		PLAYER_MAX_POS_Z = 8;






