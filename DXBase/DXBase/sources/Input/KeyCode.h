#pragma once

// 入力キーの列挙
enum class KeyCode {
	UP,
	DOWN,
	RIGHT,
	LEFT,
	ESCAPE,
	SPACE,
	RETURN,
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	L_SHIFT,
	R_SHIFT
};

// 入力ボタンの列挙
enum Buttons {
	BUTTON_UP,
	BUTTON_DOWN,
	BUTTON_RIGHT,
	BUTTON_LEFT,
	BUTTON_CIRCLE,
	BUTTON_CROSS,
	BUTTON_SQUARE,
	BUTTON_TRIANGLE,
	BUTTON_START,
	BUTTON_L1,
	BUTTON_R1,
	BUTTON_L2,
	BUTTON_R2
};

