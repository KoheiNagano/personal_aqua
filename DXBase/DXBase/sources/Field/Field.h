#pragma once

// フィールドクラス
class Field {
public:
	// コンストラクタ
	Field(unsigned int field, unsigned int skybox);
	// 更新
	void update(float deltaTime);
	// 描画
	void draw() const;
	// フィールドのハンドル取得
	unsigned int modelHandle();
	// コピー禁止
	Field(const Field& other) = delete;
	Field& operator = (const Field& other) = delete;
private:
	unsigned int mField;
	unsigned int mSkyBox;
};
