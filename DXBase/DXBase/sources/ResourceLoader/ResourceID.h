#pragma once

// リソースのIDを列挙

enum class ModelID {
	PLAYER,
	ENEMY,
	ENEMY_SWORD,
	ENEMY_SHIELD,
	BOSS,
	BOSS_EQUIP,
	STAGE,
	STAGE_COLL,
	SKYDOME
};


enum class TextureID {
	Player,
	METER_WINGS,
	METER_FRAME,
	METER_BLOOD,
	TITLE_BACK,
	TITLE_CLOUD,
	TITLE_LOGO,
	TITLE_BUTTON
};

enum class MaskID{
	METER
};

enum class AnimationID {
	Player
};

enum class SoundID {
	Player
};
