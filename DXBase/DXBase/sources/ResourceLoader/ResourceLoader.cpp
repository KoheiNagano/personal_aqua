#include "ResourceLoader.h"

#include <DxLib.h>

#include "../Math/Math.h"

// コンストラクタ
ResourceLoader::ResourceLoader(){}

// デストラクタ
ResourceLoader::~ResourceLoader(){
	clearResources();
}

// インスタンス
ResourceLoader & ResourceLoader::GetInstance(){
	static ResourceLoader instance;
	return instance;
}

// モデルのロード
void ResourceLoader::loadModel(const ModelID & id, const char * file_name){
	mModels[id] = MV1LoadModel(file_name);
}

// テクスチャのロード
void ResourceLoader::loadTexture(const TextureID & id, const char * file_name){
	mTextures[id] = LoadGraph(file_name);
}

void ResourceLoader::loadMask(const MaskID & id, const char * file_name){
	mMasks[id] = LoadMask(file_name);
}

// アニメーションのロード(縦横同比率)
void ResourceLoader::loadAnimation(const AnimationID & id, const char * file_name, const int & size, const int & row, const int & column, const int & surplus) {
	loadAnimation(id, file_name, Vector2::One * size, row, column, surplus);
}

// アニメーションのロード(縦横異比率)
void ResourceLoader::loadAnimation(const AnimationID & id, const char * file_name, const Vector2 & size, const int & row, const int & column, const int & surplus){
	// 画像を分割して一つのアニメーションとして格納
	int handle[100];
	LoadDivGraph(file_name, (row*column) - surplus, row, column, size.x, size.y, handle);
	for (int i = 0; i < (row*column) - surplus; i++) {
		mAnimations[id].push_back(handle[i]);
	}
}

// サウンドのロード
void ResourceLoader::loadSound(const SoundID & id, const char * file_name){
	int handle = LoadSoundMem(file_name);

	if (handle == -1) throw std::string("ファイルは存在しません");

	mSounds[id] = handle;
}

// モデルハンドルの取得
int ResourceLoader::getModelHandle(ModelID id){
	return mModels[id];
}

// テクスチャハンドルの取得
int ResourceLoader::getTextureHandle(TextureID id){
	return mTextures[id];
}

int ResourceLoader::getMaskHandle(MaskID id){
	return mMasks[id];
}

// サウンドハンドルの取得
int ResourceLoader::getSoundHandle(const SoundID & id){
	return mSounds[id];
}

// アニメーションハンドルの取得
std::vector<int> ResourceLoader::getAnimationHandles(const AnimationID & id) {
	return mAnimations[id];
}

// テクスチャのサイズ取得
Vector2 ResourceLoader::getTexSize(TextureID id){
	int size_x, size_y;
	GetGraphSize(mTextures[id], &size_x, &size_y);
	return Vector2(static_cast<float>(size_x), static_cast<float>(size_y));
}

// アニメーションのサイズ取得
Vector2 ResourceLoader::getTexSize(AnimationID id){
	int size_x, size_y;
	GetGraphSize(mAnimations[id][0], &size_x, &size_y);
	return Vector2(static_cast<float>(size_x), static_cast<float>(size_y));
}

// リソースの解放
void ResourceLoader::clearResources(){
	mModels.clear();
	mTextures.clear();
	mMasks.clear();
	mSounds.clear();
	mAnimations.clear();
}
