#ifndef _RESOURCE_LOADER_
#define _RESOURCE_LOADER_

#include "ResourceID.h"

#include <unordered_map>
#include <string>

struct Vector2;

// リソースローダクラス
class ResourceLoader{
private:
	// コンストラクタ
	ResourceLoader();
	// デストラクタ
	~ResourceLoader();
public:
	// インスタンス
	static ResourceLoader& GetInstance();
	// モデルのロード
	void loadModel(const ModelID& id, const char* file_name);
	// テクスチャのロード
	void loadTexture(const TextureID& id, const char* file_name);
	// マスクのロード
	void loadMask(const MaskID& id, const char* file_name);
	// アニメーションのロード(縦横同比率)
	void loadAnimation(const AnimationID& id, const char* file_name, const int & size, const int & row, const int & column, const int & surplus = 0);
	// アニメーションのロード(縦横異比率)	
	void loadAnimation(const AnimationID& id, const char* file_name, const Vector2 & size, const int & row, const int & column, const int & surplus = 0);
	// サウンドのロード
	void loadSound(const SoundID& id, const char* file_name);

	// モデルハンドルの取得
	int getModelHandle(ModelID id);
	// テクスチャハンドルの取得
	int getTextureHandle(TextureID id);
	// マスクハンドルの取得
	int getMaskHandle(MaskID id);
	// サウンドハンドルの取得
	int getSoundHandle(const SoundID& id);
	// アニメーションハンドルの取得
	std::vector<int> getAnimationHandles(const AnimationID& id);

	// テクスチャのサイズ取得
	Vector2 getTexSize(TextureID id);
	// アニメーションのサイズ取得
	Vector2 getTexSize(AnimationID id);

	// リソースの解放
	void clearResources();
private:
	// モデルハンドルのコンテナ
	std::unordered_map<ModelID, int> mModels;
	// テクスチャハンドルのコンテナ
	std::unordered_map<TextureID, int> mTextures;
	// マスクハンドルのコンテナ
	std::unordered_map<MaskID, int> mMasks;
	// サウンドハンドルのコンテナ
	std::unordered_map<SoundID, int> mSounds;
	// アニメーションハンドルのコンテナ
	std::unordered_map<AnimationID, std::vector<int>> mAnimations;
};

#endif
