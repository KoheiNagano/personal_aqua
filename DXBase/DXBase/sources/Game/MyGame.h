#pragma once

#include "Game.h"
#include "GameDevice.h"

// ゲームアプリケーションクラス
class MyGame : public Game{
public:
	// コンストラクタ
	MyGame();
	// デストラクタ
	~MyGame();
	// 初期化
	void start();
	// 更新処理
	void update();
	// 描画処理
	void draw();
	// 終了処理
	void end();
private:
	// ゲームデバイス
	GameDevice mGameDevice;
};
