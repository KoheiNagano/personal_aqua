#include "MyGame.h"
#include "../Define.h"

// コンストラクタ
MyGame::MyGame() : Game(SCREEN_SIZE, WINDOW_RATE, WINDOW_MODE) {}

// デストラクタ
MyGame::~MyGame(){}

// 初期化
void MyGame::start(){
	mGameDevice.start();
}

// 更新処理
void MyGame::update(){
	mGameDevice.update();
}

// 描画処理
void MyGame::draw(){
	mGameDevice.draw();
}

// 終了処理
void MyGame::end(){
	mGameDevice.end();
}