#include "GameDevice.h"

#include "../Scene/LoadingScene/LoadingScene.h"
#include "../Scene/Title/TitleScene.h"
#include "../Scene/GamePlay/GamePlayScene.h"
#include "../Scene/GameOver/GameOverScene.h"

#include "../Scene/Base/SceneID.h"
#include "../Game/Time.h"
#include "../Define.h"

// コンストラクタ
GameDevice::GameDevice(){}

// デストラクタ
GameDevice::~GameDevice() {}

// 初期化
void GameDevice::start(){
	// シーンの登録
	mSceneMgr.add(SceneID::Loading, std::make_shared<LoadingScene>(mSceneMgr.getData()));
	mSceneMgr.add(SceneID::Title, std::make_shared<TitleScene>(mSceneMgr.getData()));
	mSceneMgr.add(SceneID::GamePlay, std::make_shared<GamePlayScene>(mSceneMgr.getData()), STAGE1_PATH);
	mSceneMgr.add(SceneID::GameOver, std::make_shared<GameOverScene>(mSceneMgr.getData()));
	mSceneMgr.change(SceneID::Loading);
}

// 更新処理
void GameDevice::update(){
	mSceneMgr.update(Time::GetInstance().deltaTime());
}

// 描画処理
void GameDevice::draw(){
	mSceneMgr.draw();
}

// 終了処理
void GameDevice::end(){
	mSceneMgr.end();
}