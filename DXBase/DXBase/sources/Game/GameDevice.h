#pragma once

#include "DxLib.h"
#include "../Scene/Base/SceneMgr.h"

// ゲームデバイスクラス
class GameDevice{
public:
	// コンストラクタ
	GameDevice();
	// デストラクタ
	~GameDevice();
	// 初期化
	void start();
	// 更新処理
	void update();
	// 描画処理
	void draw();
	// 終了処理
	void end();
private:
	// コピー禁止
	GameDevice(const GameDevice& other) = delete;
	GameDevice& operator = (const GameDevice& other) = delete;
private:
	// シーンマネージャー
	SceneMgr mSceneMgr;
};
