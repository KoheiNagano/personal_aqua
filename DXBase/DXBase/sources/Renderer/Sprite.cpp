#include "Sprite.h"

using namespace std;

Sprite::Sprite(){}

Sprite::Sprite(int id, int row, int column):
id_(id), anim_num_(0), row_(row), column_(column), timer_(0), speed_(1){
	registSprite();
}

void Sprite::registSprite(){
	//分割前の画像サイズ
	Vector2 size = getSize();
	//分割した際の個々の画像サイズ
	Vector2 divSize = Vector2(size.x / row_, size.y / column_);
	//配列の確保
	sprites_ = vector<vector<int>>(column_, vector<int>(row_));
	//分割後のidを格納
	for (int i = 0; i < column_; ++i) {
		for (int j = 0; j < row_; ++j) {
			Vector2 src = Vector2(size.x / row_ * j, size.y / column_ * i);
			sprites_[i][j] = DerivationGraph(src.x, src.y, divSize.x, divSize.y, id_);
		}
	}
}

void Sprite::change(int anim_num, float speed){
	//再生速度を変更
	speed_ = speed;

	//アニメーションが同じ場合はreturn
	if (anim_num_ == anim_num)return;
	//指定したアニメーションに変更
	anim_num_ = anim_num;
	//再生時間をリセット
	timer_ = 0;
}

void Sprite::update(float deltaTime){
	//更新処理
	timer_ += deltaTime * speed_ * 10 / row_;
}

void Sprite::draw(Vector2 position) const {
	DrawGraph(position.x, position.y, sprites_[anim_num_][static_cast<int>(timer_) % row_], TRUE);
}

Vector2 Sprite::getSize(){
	int size_x, size_y;
	GetGraphSize(id_, &size_x, &size_y);
	return Vector2(size_x, size_y);
}
